\input{include/header.tex}

% auto-generated glossary terms
\input{notes/2.glt}

% rules of inference
\input{include/theories/ipl.tex}


\indextitle{A fragment of propositional logic}
\subtitle{Our first real proofs}

\makeglossaries


\begin{document}

\maketitle
\tableofcontents

\newpage


\section{Introduction}

  In the study of \vocab{proof-theory}, there is not actually a single universal \vocab{proof-system}, but a variety of different systems that are related to each other in subtle ways.

  \longdemo{Philosophical aside: how do we know which proof system is ``right'' to use in the ``real world''?}

  We will investigate several different \vocabs{proof-system} in this course, increasing in complexity as we progress. For our very first proof system, we will study a simple \vocab{fragment} of a system known as \vocabstyle{\acrfull{ipl}}. The term \vocab{fragment} in this context means that we will not be able to prove quite everything that we ``should'' be able to prove in \acrshort{ipl}, just some subset.

  This proof system won't let us prove anything very interesting just yet, but it will help us establish the mechanics of how to work with a proof system in general. In these notes, we'll learn how to construct \vocabs{proof-tree} by applying \vocabs{rule-of-inference}, which will be defined in terms of \vocab{unification} and \vocab{substitution} over \vocabs{proposition}.



\section{Definitions}

  \subsection{Operators}

    Our proof system will use these operators, with pronunciations and meanings that will be introduced as we go through these notes:

    \begin{itemize}
      \item $\position{\vocab{sym-wedge}}\position$
      \item $\position{\vocab{sym-vee}}\position$
      \item $\position{\vocab{sym-supset}}\position$
      \item $\vocab{sym-top}$
    \end{itemize}

    Note that $\vocab{sym-top}$ is a \vocab{constant} with no positions.


  \subsection{Propositions}

    We define a \vocab{proposition} in our proof system to be any \vocab{closed} \vocab{expression} using the operators listed above. These are the \insist{only} operators with \vocabs{position} that a proposition may use.

    We allow our \vocabs{proposition} to include any arbitrary \vocabs{constant}, like \mono{P}, \mono{FOO}, or $\Phi$. In these notes, we will generally write these constants in uppercase to make them especially obvious. \vocab{sym-top} is a \insist{special} constant that will have special meaning in our proof rules. (It is not an upper-case \mono{T}, but a special character of its own.)

    \longdemo{What are some example propositions that we can construct with this definition?}

  
  \subsection{Rules of inference}

    A proof system is a kind of puzzle game, and \vocabs{rule-of-inference} (or \vocabstyle{inference rules}) are the ``moves'' that a player is allowed to make in the game. A proof system is \insist{defined} by which rules of inference it allows.

    A rule of inference is traditionally written in a two-dimensional notation: a horizontal line dividing two rows of symbols, with a name next to one of the endpoints of the line. (It looks kind of like a fraction, but it's not a fraction.) The general structure looks like this:

    \begin{center}
      \AxiomC{$\vocabs{premise}$}
      \LeftLabel{$\rulename{RuleName}$}
      \UnaryInfC{$\vocab{conclusion}$}
      \DisplayProof
    \end{center}

    It doesn't matter whether we write the rule name on the left or right of the line:\\
      \AxiomC{$\vocabs{premise}$}
      \LeftLabel{$\rulename{RuleName}$}
      \UnaryInfC{$\vocab{conclusion}$}
      \DisplayProof
    and
      \AxiomC{$\vocabs{premise}$}
      \RightLabel{$\rulename{RuleName}$}
      \UnaryInfC{$\vocab{conclusion}$}
      \DisplayProof
    have the same meaning.

    Underneath the line, there is a \insist{single} \vocab{expression} called the \vocab{conclusion}. On top of the line, there may be \insist{any natural number of} \vocabs{expression} called the \vocabs{premise} (including zero). When an inference rule has no premises, we sometimes call it an \vocab{axiom}. When describing an inference rule, we say the conclusion ``follows from'' the premises.

    Here are what the structures of zero-, one-, two-, and three-premise inference rules look like:

    \begin{center}
      \AxiomC{}
      \LeftLabel{$\rulename{RuleName}$}
      \UnaryInfC{$\vocab{conclusion}$}
      \DisplayProof

      \AxiomC{$\vocab{premise}_1$}
      \LeftLabel{$\rulename{RuleName}$}
      \UnaryInfC{$\vocab{conclusion}$}
      \DisplayProof

      \AxiomC{$\vocab{premise}_1$}
      \AxiomC{$\vocab{premise}_2$}
      \LeftLabel{$\rulename{RuleName}$}
      \BinaryInfC{$\vocab{conclusion}$}
      \DisplayProof

      \AxiomC{$\vocab{premise}_1$}
      \AxiomC{$\vocab{premise}_2$}
      \AxiomC{$\vocab{premise}_3$}
      \LeftLabel{$\rulename{RuleName}$}
      \TrinaryInfC{$\vocab{conclusion}$}
      \DisplayProof
    \end{center}

    When writing proofs, we can read inference rules in two ways:
    \begin{itemize}
      \item Top-to-bottom: if we \insist{have} proofs for the premises of an inference rule, then we can use the rule to \vocab{construct} a proof of the conclusion.
      \item Bottom-to-top: if we \insist{want to} prove the conclusion of an inference rule, then we can use the rule to \vocab{split} (or ``deconstruct'') our problem into subproblems: now we have to construct a proof of each premise.
    \end{itemize}

    The premises and conclusion in an inference rule are \vocabs{proposition} which may be \vocab{open}. Here is an example that we'll revisit later in these notes:

    \begin{center}
      \example{\AndElimL}
    \end{center}

    The $\wedge$ operator is our logical ``and'' operator. From top to bottom, the \andElimL\ rule says that if we have a single proof for both \metavar{a} \insist{and} \metavar{b}, then we can construct a proof of \metavar{a}. From bottom to top, the \andElimL\ rule says that if we want to prove \metavar{a}, we can do it by constructing a single proof for both \metavar{a} and \metavar{b}.


  \subsection{Proof trees}

    A \vocab{proof-tree} is a particular kind of tree structure that we construct by following rules of inference. This is where \vocab{unification} comes into play, and where we get our first formal definition of the word \vocab{proof}.
    
    Proof trees look a little different than traditional tree diagrams in computer science: for one thing, a proof tree ``grows'' \insist{up} with its root at the \insist{bottom}, like most actual trees in nature. A proof tree is made of multiple inference rules piled on top of each other in a branching structure.

    When we start constructing a proof tree, we start with some \vocab{proposition} that we want to prove. This proposition must be \vocab{closed}: the inference rules of our system may have \vocabs{metavariable}, but our proof trees can't.

    At each step of constructing the tree, we must choose an inference rule whose \vocab{conclusion} \vocabs{unify-with} the proposition that we're trying to prove. There may be more than one to choose from. Unlike in our unification algorithm, it \insist{is} possible to make a ``wrong'' choice, so we are allowed to ``undo'' our choice later if we want to.

    When we choose a rule, we take the \vocab{unifying-substitution} that we got from unifying the rule's conclusion with our proposition: call it $\sigma$. To construct each next \vocab{branch} of our proof tree, we \vocab{substitute} with $\sigma$ in each \vocab{proposition} of the inference rule (if there are any).

    For example:

    \longexample{
      proposition: $\mono{P} \wedge \mono{Q}$

      \medskip

      rule: \AndIntro

      \medskip

      unification problem: $\left\{\begin{array}{r c l} \metavar{a} \wedge \metavar{b} & \unifyequals & \mono{P} \wedge \mono{Q} \end{array}\right\}$

      \medskip

      unifying substitution: $\sigma := \begin{cases}
        \metavar{a} \mapsto & \mono{P} \\
        \metavar{b} \mapsto & \mono{Q}
      \end{cases}$

      \medskip

      proof tree:
        \AxiomC{$\metavar{a}[\sigma]$}
        \AxiomC{$\metavar{b}[\sigma]$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\left(\metavar{a} \wedge \metavar{b}\right)[\sigma]$}
        \DisplayProof
        \quad which is \quad
        \AxiomC{\mono{P}}
        \AxiomC{\mono{Q}}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\mono{P} \wedge \mono{Q}$}
        \DisplayProof
    }

    We repeat this process for each proposition in our proof tree that does not have a line over it. We can do this in any order, and there is no ``wrong'' order. This is what gives rise to the tree structure of our proof tree. For example:

    \longexample{
      proposition: $\left(\mono{P} \wedge \mono{Q}\right) \wedge \left(\mono{R} \wedge \mono{S}\right)$

      \medskip

      rule: \AndIntro

      \medskip

      unification problem: $\left\{\begin{array}{r c l} \metavar{a} \wedge \metavar{b} & \unifyequals & \left(\mono{P} \wedge \mono{Q}\right) \wedge \left(\mono{R} \wedge \mono{S}\right) \end{array}\right\}$

      \medskip

      unifying substitution: $\sigma := \begin{cases}
        \metavar{a} \mapsto & \mono{P} \wedge \mono{Q} \\
        \metavar{b} \mapsto & \mono{R} \wedge \mono{S}
      \end{cases}$

      \medskip

      proof tree:
          \AxiomC{$\mono{P} \wedge \mono{Q}$}
          \AxiomC{$\mono{R} \wedge \mono{S}$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\left(\mono{P} \wedge \mono{Q}\right) \wedge \left(\mono{R} \wedge \mono{S}\right)$}
        \DisplayProof

      \medskip

      rule: \andIntro

      \medskip

      unification problem: $\left\{\begin{array}{r c l} \metavar{a} \wedge \metavar{b} & \unifyequals & \mono{P} \wedge \mono{Q} \end{array}\right\}$

      \medskip

      unifying substitution: $\sigma := \begin{cases}
        \metavar{a} \mapsto & \mono{P} \\
        \metavar{b} \mapsto & \mono{Q}
      \end{cases}$

      \medskip

      proof tree:
            \AxiomC{$\mono{P}$}
            \AxiomC{$\mono{Q}$}
          \LeftLabel{\andIntro}
          \BinaryInfC{$\mono{P} \wedge \mono{Q}$}
          \AxiomC{$\mono{R} \wedge \mono{S}$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\left(\mono{P} \wedge \mono{Q}\right) \wedge \left(\mono{R} \wedge \mono{S}\right)$}
        \DisplayProof

      \medskip

      rule: \andIntro

      \medskip

      unification problem: $\left\{\begin{array}{r c l} \metavar{a} \wedge \metavar{b} & \unifyequals & \mono{R} \wedge \mono{S} \end{array}\right\}$

      \medskip

      unifying substitution: $\sigma := \begin{cases}
        \metavar{a} \mapsto & \mono{R} \\
        \metavar{b} \mapsto & \mono{S}
      \end{cases}$

      \medskip

      proof tree:
            \AxiomC{$\mono{P}$}
            \AxiomC{$\mono{Q}$}
          \LeftLabel{\andIntro}
          \BinaryInfC{$\mono{P} \wedge \mono{Q}$}
            \AxiomC{$\mono{R}$}
            \AxiomC{$\mono{S}$}
          \LeftLabel{\andIntro}
          \BinaryInfC{$\mono{R} \wedge \mono{S}$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\left(\mono{P} \wedge \mono{Q}\right) \wedge \left(\mono{R} \wedge \mono{S}\right)$}
        \DisplayProof
    }


  \subsection{Errors}

    What can go wrong when we try to construct a \vocab{proof-tree}?

    If we choose an invalid \vocablink{rule-of-inference}{inference rule} at any step, our \vocab{unification-problem} will not have a \vocab{unifying-substitution}. Let's start in the middle of constructing that last proof tree:

    \longexample{
      proof tree:
            \AxiomC{$\mono{P}$}
            \AxiomC{$\mono{Q}$}
          \LeftLabel{\andIntro}
          \BinaryInfC{$\mono{P} \wedge \mono{Q}$}
          \AxiomC{$\mono{R} \wedge \mono{S}$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\left(\mono{P} \wedge \mono{Q}\right) \wedge \left(\mono{R} \wedge \mono{S}\right)$}
        \DisplayProof
    }

    We've seen that we can use \andIntro\ on the $\mono{R} \wedge \mono{S}$ premise. What if we tried to use a different rule?

    Let's consider this rule as a potential option:

    \longexample{
      rule: \OrIntroL

      \medskip

      unification problem: $\left\{\begin{array}{r c l} \metavar{a} \vee \metavar{b} & \unifyequals & \mono{R} \wedge \mono{S} \end{array}\right\}$

      \medskip

      unifying substitution: ?
    }

    What does our \vocab{unification-algorithm} tell us here? We get a ``no'' answer: there is no unifying substitution for our unification problem.

    \longexample{
      unifying substitution: \textcolor{red}{none}
    }

    This means that the \orIntroL\ rule is \insist{invalid} to use in this part of this proof tree.

    To \vocab{check} a proof, we just work through the unification problems that are represented by the rules in the tree, checking that the premises match the conclusion according to the definition of the rule being used.

    
  \subsection{Complete proofs}

    A proof tree is \vocablink{complete-proof-tree}{complete} when every proposition in the tree has a line above it. For example, the \topIntro\ rule allows us to put a line over $\top$ if it is the only symbol in a proposition:


    \longexample{
      rules: \AndIntro \quad \TopIntro

      proposition: $\left(\top \wedge \top\right) \wedge \left(\top \wedge \top\right)$

      proof tree:
              \AxiomC{}
            \LeftLabel{\topIntro}
            \UnaryInfC{$\top$}
              \AxiomC{}
            \RightLabel{\topIntro}
            \UnaryInfC{$\top$}
          \LeftLabel{\andIntro}
          \BinaryInfC{$\top \wedge \top$}
              \AxiomC{}
            \LeftLabel{\topIntro}
            \UnaryInfC{$\top$}
              \AxiomC{}
            \RightLabel{\topIntro}
            \UnaryInfC{$\top$}
          \RightLabel{\andIntro}
          \BinaryInfC{$\top \wedge \top$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\left(\top \wedge \top\right) \wedge \left(\top \wedge \top\right)$}
        \DisplayProof
    }

    When we have a \vocab{complete-proof-tree} for some proposition, we say we have a \vocab{proof} of the proposition.

    In most proof systems, it is possible for a single proposition to have multiple different proofs. We'll investigate this in some depth later when we discuss the topic of \vocab{normalization}.


  \subsection{Assumptions}

    We sometimes write propositions with \vocabs{assumption}, indicating specific premises that may be used without a proof. This can be read as a \insist{problem statement}: we're expressing exactly what needs to be proven and what can be left unproven.

    When we write assumptions in these notes, we will use three vertical dots to indicate them. For example:

    \begin{center}
      \AxiomC{\assumption{\mono{P}}}
      \AxiomC{\assumption{\mono{Q}}}
      \BinaryInfC{$\mono{P} \wedge \left(\mono{Q} \wedge \top\right)$}
      \DisplayProof
    \end{center}

    A solution to this problem is a \vocab{proof} with the conclusion $\mono{P} \wedge \left(\mono{Q} \wedge \top\right)$, which gets to use a special \vocab{axiom} called \assumed\ to prove \mono{P} or \mono{Q}. For example:

    \longexample{
      rules: \AndIntro \quad \TopIntro

      proposition:
        \AxiomC{\assumption{\mono{P}}}
        \AxiomC{\assumption{\mono{Q}}}
        \BinaryInfC{$\mono{P} \wedge \left(\mono{Q} \wedge \top\right)$}
        \DisplayProof

      proof tree:
            \AxiomC{}
          \LeftLabel{\assumed}
          \UnaryInfC{\mono{P}}
              \AxiomC{}
            \LeftLabel{\assumed}
            \UnaryInfC{\mono{Q}}
              \AxiomC{}
            \RightLabel{\topIntro}
            \UnaryInfC{$\top$}
          \RightLabel{\andIntro}
          \BinaryInfC{$\mono{Q} \wedge \top$}
        \RightLabel{\andIntro}
        \BinaryInfC{$\mono{P} \wedge \left(\mono{Q} \wedge \top\right)$}
        \DisplayProof
    }

    Note that we may \insist{only} use \assumed\ to prove \mono{P} and \mono{Q} because the proposition explicitly stated them as assumptions. We could not use \assumed\ in this problem to prove $\top$, or \mono{R}, or even $\mono{P} \wedge \mono{Q}$: we can only use it to prove exactly \mono{P} or exactly \mono{Q}. If we use \assumed\ with a different conclusion, we may have produced a solution for some other problem, but not for this problem.

    Each assumption may be used any number of times, including not at all. For example:

    \longexample{
      rules: \AndIntro

      proposition:
        \AxiomC{\assumption{\mono{P}}}
        \AxiomC{\assumption{\mono{Q}}}
        \BinaryInfC{$\mono{P} \wedge \mono{P}$}
        \DisplayProof

      proof tree:
          \AxiomC{}
        \LeftLabel{\assumed}
        \UnaryInfC{\mono{P}}
          \AxiomC{}
        \RightLabel{\assumed}
        \UnaryInfC{\mono{P}}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\mono{P} \wedge \mono{P}$}
        \DisplayProof
    }

    We will have a more thorough treatment of assumptions soon. For now, we'll just use this assumption notation to represent problems for the sake of exercise.



\section{Proof system}

  
  \subsection{Operators}

    Here are the pronunciations and logical meanings of each operator in our proof system:

    \begin{itemize}
      \item $\position{\wedge}\position$ is ``and'', the \vocab{conjunction} operator
      \item $\position{\vee}\position$ is ``or'', the \vocab{disjunction} operator
      \item $\position{\subset}\position$ is ``implies'', the \vocab{implication} operator
      \item $\top$ is ``top'', the proposition that's \vocab{trivial} to prove
    \end{itemize}

    The precise meaning of each operator in our proof system is unambiguously defined by the \vocabs{rule-of-inference} in our proof system: any English term that we assign to the operator is an approximation, which is occasionally important to be aware of.

    \longdemo{What are some different meanings of ``and'', ``or'', and ``implies'' in English? Which meanings are closest to how these operators behave in our proof system? (We'll discuss this question as we read through the rules below.)}


  \subsection{Rules}

    Here are all of the \vocabs{rule-of-inference} in our proof system for now. Keep in mind, these rules are enough to prove some propositions in \vocabstyle{\acrshort{ipl}}, but not all---we're just warming up. In the next notes, we will introduce the concept of a \vocab{context} of \vocabs{assumption} so that we can finish introducing the inference rules for \acrshort{ipl}.

    \begin{center}
      \TopIntro

      \AndIntro \quad \AndElimL \quad \AndElimR

      \OrIntroL \quad \OrIntroR

      \ImplElim
    \end{center}

    These rule names use a traditional naming convention:

    \begin{itemize}
      \item \rulename{Intro} is short for \vocab{introduction-rule}, a rule which has its respective operator in its \vocab{conclusion} and not in its \vocabs{premise}.
      \item \rulename{Elim} is short for \vocab{elimination-rule}, a rule which has its respective operator in one of its \vocabs{premise} and not in its \vocab{conclusion}.
      \item The $_L$ and $_R$ subscripts are short for ``left'' and ``right''.
    \end{itemize}

    You might recognize some of these rules as logical principles that you've seen before, and if so you might remember some Latin names for them. In this course, we will generally refer to these rules by the names listed above, which are relatively traditional for proof theory.

    \longdemo{What's the interpretation of each of these rules top-to-bottom and bottom-to-top?}

    When we're writing a \vocab{proof-tree} in a particular \vocab{proof-system}, we must \insist{only} use the \vocabs{inference-rule} provided by the proof system. Making up new inference rules can be a productive form of exploration and research, but it means you're working in a fundamentally different proof system than you started with, and therefore any proof you construct might not be valid in the original proof system.



  \subsection{Examples}

    Here are some example proofs that we can build with the rules defined in the previous section.

    \begin{center}
      \example{
        proposition: 
          \AxiomC{\assumption{\mono{P}}}
          \AxiomC{\assumption{\mono{S}}}
          \BinaryInfC{$\left(\mono{P} \vee \mono{Q}\right) \wedge \left(\mono{R} \vee \mono{S}\right)$}
          \DisplayProof

        \medskip

        proof tree:
                \AxiomC{}
              \LeftLabel{\assumed}
              \UnaryInfC{\mono{P}}
            \LeftLabel{\orIntroL}
            \UnaryInfC{$\mono{P} \vee \mono{Q}$}
                \AxiomC{}
              \RightLabel{\assumed}
              \UnaryInfC{\mono{S}}
            \RightLabel{\orIntroR}
            \UnaryInfC{$\mono{R} \vee \mono{S}$}
          \LeftLabel{\andIntro}
          \BinaryInfC{$\left(\mono{P} \vee \mono{Q}\right) \wedge \left(\mono{R} \vee \mono{S}\right)$}
          \DisplayProof
      }
      \example{
        proposition: 
          \AxiomC{\assumption{$\mono{P} \subset \mono{Q}$}}
          \AxiomC{\assumption{$\mono{Q} \subset \mono{R}$}}
          \AxiomC{\assumption{$\mono{P}$}}
          \TrinaryInfC{\mono{R}}
          \DisplayProof

        proof tree:
              \AxiomC{}
            \LeftLabel{\assumed}
            \UnaryInfC{$\mono{Q} \subset \mono{R}$}
                \AxiomC{}
              \LeftLabel{\assumed}
              \UnaryInfC{$\mono{P} \subset \mono{Q}$}
                \AxiomC{}
              \RightLabel{\assumed}
              \UnaryInfC{\mono{P}}
            \RightLabel{\implElim}
            \BinaryInfC{\mono{Q}}
          \LeftLabel{\implElim}
          \BinaryInfC{\mono{R}}
          \DisplayProof
      }
      \example{
        proposition: 
          \AxiomC{\assumption{$\mono{P} \subset \left(\mono{Q} \subset \mono{R}\right)$}}
          \AxiomC{\assumption{$\mono{P} \wedge \mono{Q}$}}
          \BinaryInfC{$\mono{R}$}
          \DisplayProof

        proof tree:
                \AxiomC{}
              \LeftLabel{\assumed}
              \UnaryInfC{$\mono{P} \subset \left(\mono{Q} \subset \mono{R}\right)$}
                  \AxiomC{}
                \RightLabel{\assumed}
                \UnaryInfC{$\mono{P} \wedge \mono{Q}$}
              \RightLabel{\andElimL}
              \UnaryInfC{$\mono{P}$}
            \LeftLabel{\implElim}
            \BinaryInfC{$\mono{Q} \subset \mono{R}$}
                \AxiomC{}
              \RightLabel{\assumed}
              \UnaryInfC{$\mono{P} \wedge \mono{Q}$}
            \RightLabel{\andElimR}
            \UnaryInfC{$\mono{Q}$}
          \LeftLabel{\implElim}
          \BinaryInfC{\mono{R}}
          \DisplayProof
      }
      \demo{
        proposition: 
          \AxiomC{\assumption{$\mono{P} \wedge \mono{Q}$}}
          \UnaryInfC{$\mono{Q} \wedge \mono{P}$}
          \DisplayProof

        proof tree: ?
      }
      \demo{
        proposition: 
          \AxiomC{\assumption{$\mono{P} \wedge \left(\mono{Q} \wedge \mono{R}\right)$}}
          \UnaryInfC{$\left(\mono{P} \wedge \mono{Q}\right) \wedge \mono{R}$}
          \DisplayProof

        proof tree: ?
      }
      \demo{
        proposition: 
          \AxiomC{\assumption{$\mono{P} \subset \mono{Q}$}}
          \AxiomC{\assumption{$\mono{P} \subset \mono{R}$}}
          \AxiomC{\assumption{$\mono{P}$}}
          \TrinaryInfC{$\mono{Q} \wedge \mono{R}$}
          \DisplayProof

        proof tree: ?
      }
    \end{center}



  \section{Meaning}

    What can we do with a proof in \vocabstyle{\acrshort{ipl}}? What do these \vocab{constants} like \mono{P} and \mono{Q} actually \insist{mean} to us?

    In general, we can define the constants in a proof to ``mean'' anything we want, and if we define them sensibly then we get a sensible statement. For example:

    \longexample{
      proposition: 
        \AxiomC{\assumption{$\mono{P} \wedge \mono{Q}$}}
        \UnaryInfC{$\mono{Q} \wedge \mono{P}$}
        \DisplayProof

      proof: ? (worked through in lecture)

      meaning:
        
      Let $\mono{P}$ mean ``$1 + 1 = 2$''.

      Let $\mono{Q}$ mean ``$3 + 3 = 6$''.

      ``Assuming $1 + 1 = 2$ and $3 + 3 = 6$, we can conclude $3 + 3 = 6$ and $1 + 1 = 2$.''
    }

    This is not very exciting yet, but it is valid! As our proof system becomes more powerful, we will gradually become able to express and prove more interesting propositions.

    Remember though, these sorts of ``meaning translations'' are only trustworthy as long as we keep in mind that our intuitive reading of words like ``and'' may not match the behavior of our proof system in all scenarios. Here is a classic example of where the word ``and'' can be misleading:

    \longexample{
      proposition: 
        \AxiomC{\assumption{$\mono{P}$}}
        \UnaryInfC{$\mono{P} \wedge \mono{P}$}
        \DisplayProof

      proof:
            \AxiomC{}
          \LeftLabel{\assumed}
          \UnaryInfC{$\mono{P}$}
            \AxiomC{}
          \RightLabel{\assumed}
          \UnaryInfC{$\mono{P}$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\mono{P} \wedge \mono{P}$}
        \DisplayProof

      meaning:
        
      Let \mono{P} mean ``I have one dollar''.

      ``Assuming I have one dollar, we can conclude that I have one dollar and I have one dollar.''
    }

    This sentence is true for a very specific meaning of the word ``and'', but not necessarily the meaning you would first assume when reading the sentence! You might read this sentence as saying ``if I have one dollar then I have two dollars (one dollar and one \insist{other} dollar)'', which is certainly not true in general. What the proof is actually saying is that if ``I have one dollar'' is true, then it \insist{keeps being true} even if I \insist{say} it twice (assuming I don't lose the dollar).

    In general, the form of \vocabstyle{\acrlong{ipl}} that we have right now does not allow us to reason very carefully about finite \vocab{resources} like real-world dollars. We'll eventually investigate why this is, and see some techniques for designing proof systems that are able to reason about resources in a more fine-grained way.


\input{include/footer.tex}
\end{document}

