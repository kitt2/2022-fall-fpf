{-
This set of notes covers our lecture discussions from week 8 on the topics of
finite sets and equality.
-}
module notes13 where

{-
Here's our basic STLC types again.
-}

data Unit : Set where
  unit : Unit

data Void : Set where

data _×_ (A B : Set) : Set where
  _,_ : A → B → A × B

data _⊎_ (A B : Set) : Set where
  inj₁ : A → A ⊎ B
  inj₂ : B → A ⊎ B

variable
  P Q R S : Set


{-
The "data/where" notation that we use above is formally known as "generalized
algebraic data type" (GADT) notation. When we use GADT notation to define
*logical* constructs, we give the introduction rules in the form of
constructors, and we get the elimination rules "for free" by pattern matching.

GADT notation can also be used to define *programming* constructs, as a general
form of data abstraction. For our first example, the Four type below is an
"enumeration" type with exactly four constructors, named "zero" through
"three".
-}
data Four : Set where
  zero : Four
  one : Four
  two : Four
  three : Four

{-
Like "enum" types in many programming languages, the Four type has *exactly*
four different values. If we take a Four value as an argument to a function, we
know that the argument *must* have been constructed with one of these four
constructors.

With this in mind, we can define "incrementation" (adding one) for the Four
type using pattern-matching. Our definition will have "overflow" behavior, like
most common numeric types on computers.
-}
inc : Four → Four
inc zero = one
inc one = two
inc two = three
inc three = zero

{-
In the Agda plugin, you can call this function by typing C-c C-n and entering an
expression like "inc zero" or "inc (inc (inc three))".

How about addition? Well, we could pattern-match on both arguments and define
all 16 different cases by hand, but we can do a little better:
-}
add : Four → Four → Four
add zero y = y
add one y = inc y
add two y = inc (inc y)
add three y = inc (inc (inc y))

{-
Note that if we try to change this definition to be recursive, Agda will
complain about "non-termination": we'll come back to this concept soon.

The definition of add works by pattern-matching *only* on the first argument,
and using it to decide how many times to call inc on the second argument. Try it
out with C-c C-n to see it in action.

Now that we have an addition function, it would be nice to prove some things
about addition: for example, we expect that x + y = y + x for any choice of x
and y.

In order to do this, we will need a *type* of equality proofs. Here is our first
definition of a "propositional equality type":
-}
data _≡_ : Four → Four → Set where
  refl : (x : Four) → x ≡ x

{-
Don't worry about the definition of this type too much yet: it involves several
features of Agda that we haven't seen yet, some of which we'll cover later.

For now, note that the _≡_ type has *exactly* one constructor called refl (short
for "reflexivity").

The refl constructor has a "dependent function type": it takes one argument of
type Four, and then its return type depends on what argument it was passed. To
see this, you can type C-c C-d to have Agda tell you the type of an expression,
and try expressions like "refl one" or "refl (add (inc one) two)".
-}

{-
We should try to be very precise about our two different kinds of equality:

  =  is *definitional* equality
  ≡  is *propositional* equality

Definitional equality is the most primitive kind: it's what Agda "knows" about
internally. Agda can reason about definitional equalities automatically, so we
don't need to "prove" them manually.

When we write a definition using the = symbol (like inc or add), we are
*creating* a definitional equality that Agda knows about. For example, Agda is
aware that "inc one = two" is a definitional equality.

When reasoning about definitional equality, Agda tries to "simplify" each
expression as much as possible using the equations that it knows about. For
example, the expression "add (inc two) three" simplifies to "two". This process
of "simplification" is what the C-c C-n command does.

In general, when we write expressions that only involve operators and constants,
Agda is able to reason about their equality very effectively. When our
expressions involve variables, the situation is more subtle.

For example, with the definition of "add" from above:

  Agda *does*     know    for any y, add zero x = x
  Agda *does not* know    for any y, add x zero = x

The first equation is literally written in our definition of "add" (up to
variable renaming), and the second equation is not. We only pattern-matched on
the first argument in our definition of "add", so Agda only knows how to
simplify a call to "add" if it knows what the first argument is.
-}

{-
Definitional equality is fundamentally limited, and does not cover everything
that we consider "equality" in traditional mathematics. This is why we have a
propositional equality type.

Every definitional equality is a propositional equality, but not every
propositional equality is a definitional equality.

  add zero x = x    is     a definitional equality
  add x zero = x    is not a definitional equality

  add zero x ≡ x    is     a propositional equality
  add x zero ≡ x    is     a propositional equality

The "refl" constructor precisely allows us to prove a propositional equality
when we have a definitional equality.
-}

{-
To state a proposition involving an arbitrary Four variable, we can use
dependent function type notation like in refl:
-}
-- "for any x of type Four, "add zero x" is propositionally equal to "x"
example₁ : (x : Four) → add zero x ≡ x
example₁ x = refl (add (inc (add three zero)) x)

{-
This definition of example₁ is valid because "add zero x" *simplifies* to "x",
so the two expressions are definitionally equal. In this proof it is also valid
to use "refl (add zero x)", or even "refl (add (inc (add three zero)) x)",
because all of these expressions *simplify* to "x".

In contrast, since Agda *cannot simplify* "add x zero", we *cannot* use refl
directly to prove example₂ below.
-}
-- example₂ : (x : Four) → add x zero ≡ x
-- example₂ x = refl ? -- error!

{-
More precisely, Agda cannot *unify* "add x zero" with "x", so the two
expressions are not definitionally equal. Again, note that there is no case in
the definition of "add" with a constructor on the right and a variable on the
left, so "add x zero" cannot be simplified.

Still, it should be true that "add x zero = x", right? Well, it is true, but
it's not "true by definition": we have to actually *prove* it. We will need to
pattern-match on "x" in order to show that the property is true for *every
possible* choice of "x".

This is where *propositional* equality differs from *definitional* equality:
Agda cannot automatically pattern-match when reasoning about definitional
equality. The only reason it knew "add zero x = x" is because it did *not* need
to pattern-match on "x" in order to simplify "add zero x".

When we pattern-match on an argument in a propositional equality proof, Agda
gains a *new* definitional equality to work with in each case of our proof.
-}
example₂ : (x : Four) → add x zero ≡ x
example₂ zero  = refl zero  -- in this case, x = zero  is a definitional equality
example₂ one   = refl one   -- in this case, x = one   is a definitional equality
example₂ two   = refl two   -- in this case, x = two   is a definitional equality
example₂ three = refl three -- in this case, x = three is a definitional equality

{-
In each case of example₂ we get a new *definitional* equality indicating which
constructor was used to construct "x", which is exactly the knowledge that Agda
needs in order to simplify "add x zero".

The interaction between definitional equality and propositional equality is
subtle, but it's very fundamental to the way that "equality" works in logic and
mathematics in general. Definitional equalities are the things that we *state*
or *assume* to be true, and propositional equalities are the things that we
*prove* to be true.
-}

{-
Our propositional equality type has several interesting properties of its own.
Remember that any time we call something an "equality" operator, we generally
expect it to have at least these three properties:

  reflexivity: x ≡ x
  symmetry: (x ≡ y) → (y ≡ x)
  transitivity: (x ≡ y) → (y ≡ z) → (x ≡ z)

The refl constructor itself is a proof of reflexivity: it allows us to prove
that any Four value is propositionally equal to itself.

We do have the properties of symmetry and transitivity, but we must *prove* them
in order to use them.

Reading the definition of symmetry as a function type, we are *given* a proof of
x ≡ y, and we have to *produce* a proof of y ≡ x. How can we "take apart" a
proof of x ≡ y in order to "use" it?

Pattern matching! We know that the _≡_ type has exactly one constructor, refl.
Something very interesting happens when we pattern-match on a propositional
equality *argument*: Agda *tells* us whether the two sides of the equality can
be unified or not.

In the proof below, enter p into the hole and type C-c C-c to pattern-match.
Note that *all* of the arguments change!
-}
sym : (x : Four) → (y : Four) → x ≡ y → y ≡ x
sym x y p = ?

{-
This is because pattern-matching on p causes Agda to solve the unification
problem { x ≐ y } and then *substitute* { y ↦ x } in the entire proof. Now, in
the case where you pattern-matched on refl, x and y are *definitionally* equal.
You can now solve the proof with "refl x".

Your Agda editor will probably update the definition of sym to involve periods
before two of the uses of "x". This is indicating that the values of these
arguments are *constrained* by unification: if the third argument is constructed
with "refl", then the first two arguments *must* both be definitionally equal.
-}

{-
Remember, as always, there is nothing stopping us from assuming things that
happen to be false. What happens if we pattern-match on a propositional equality
where unification *fails*?

Try it in the proof below: enter p into the hole and type C-c C-c to
pattern-match. Note that the whole proof is replaced with (), without even a =
symbol!
-}
example₃ : one ≡ two → one ≡ three
example₃ p = ?

{-
Like with the Void type, this is indicating that there are *no* possible
constructors of the type "one ≡ two". How does Agda come to this conclusion?

  1. refl is the only constructor in the definition of _≡_
  2. refl requires both sides of the ≡ side to *unify*
  3. one and two are different constructors, so unification fails
  4. refl must not be a valid constructor for "one ≡ two"
  5. there must be zero valid constructors for "one ≡ two"

In this case, we say that "one" and "two" are *definitionally inequal".
-}
