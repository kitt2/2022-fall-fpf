\input{include/header.tex}

% auto-generated glossary terms
\input{notes/5.glt}

% rules of inference
\input{include/theories/iplc.tex}


\indextitle{Structural logic}
\subtitle{Rearranging our assumptions}

\makeglossaries


\begin{document}

\maketitle
\tableofcontents

\newpage


\section{Introduction}


  When we reason formally about \vocabs{context} of \vocabs{assumption}, we sometimes find that our proof system does not allow us to use assumptions in certain ways that seem intuitively valid. In these notes, we'll identify these seemingly ``missing pieces'' by investigating the \vocab{structural} properties of our proof system as we've defined it so far.

  For a simple example of a ``missing piece'', we might recognize that a proof of a \vocab{tautology} can be rewritten into a proof of the same proposition in any arbitrary context: if we have a proof of $\emptyset \entails P$ for some proposition $P$, then for any context $C$ we can always modify that proof tree to produce a proof of $C \entails P$.

  Intuitively, this observation is saying that a tautology is true in \insist{any} context, not just the \insist{empty} context. This lines up with our intuitive understanding of a tautology as a proposition that is ``always true under any condition''.

  We haven't introduced the tools to prove something like this yet, but it \insist{is} in fact \vocab{true} about our definition of \vocabstyle{\acrfull{ipl}}, both the \vocab{fragment} that we've already introduced and the full set of rules that we haven't introduced yet. With a more powerful proof system, we could prove this fact about tautologies in our definition of \acrshort{ipl}.

  Since we know this property of tautologies is true, we might expect to be able to use it within our proofs. For example, consider this incomplete proof about three arbitrary propositions $P$, $Q$, and $R$:

  \begin{prooftree}
    \AxiomC{$\emptyset \entails P$}
    \LeftLabel{???}
    \UnaryInfC{$Q \cons R \cons \emptyset \entails P$}
  \end{prooftree}

  We can justify this step in our proof intuitively if we can prove that $P$ is a tautology, but we don't have any \vocab{rule-of-inference} that allows us to actually take this step when constructing a proof tree.

  In these notes, we'll see how to extend our proof system with a \vocab{derived-rule} that allows us to do this kind of reasoning ``internally'', within our proof trees. We'll have to do this very carefully to avoid ``nonsense proofs'', so in the process we'll examine more of our intuition for how contexts of assumptions ``should'' work.



\section{Local definitions}

  First, let's clarify a practice that we've already used a little bit. It gets very tedious to write the same context over and over, so to avoid repetition, we will often use \vocabs{local-definition} in our proofs. Since we'll be using local definitions much more often now, we should be a little more precise about how they work.

  To write a local definition we use the \vocab{sym-defequals} symbol, in the same way that we've used it in previous notes. In general, we may use local definitions for any \vocab{expression}, not just any \vocab{context}. In practice, we'll most often use local definitions for contexts just because it's so common to have a context repeated more than once in a proof.

  These definitions are \vocab{local} in the sense that they only apply in a single proof tree: we may locally define \mono{C} to mean $\mono{P} \cons \emptyset$ in one proof, and then locally define \mono{C} to mean $\mono{A} \vee \mono{B}$ (or anything else) in a different proof.

  \begin{center}
    \example{
      $\mono{C} \defequals \mono{P} \cons \emptyset$

      \begin{center}
        \AxiomC{}
        \LeftLabel{\inAt{0}}
        \UnaryInfC{$\mono{P} \in \mono{C}$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\mono{C} \entails \mono{P}$}
        \DisplayProof
      \end{center}
    }
    \example{
      $\mono{C} \defequals \mono{P} \cons \mono{Q} \cons \mono{R} \cons \emptyset$

      \begin{center}
        \AxiomC{}
        \LeftLabel{\inAt{1}}
        \UnaryInfC{$\mono{Q} \in \mono{C}$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\mono{C} \entails \mono{Q}$}
        \DisplayProof
      \end{center}
    }
  \end{center}

  In \vocab{unification}, we treat the $\defequals$ symbol as \vocab{definitional-equality}, the same as the \vocab{sym-equals} symbol. This is what allows us to solve unification problems when local definitions are involved. Intuitively, if we've defined $\mono{C} \defequals \mono{D}$, we can think of \mono{C} and \mono{D} as ``identical'' for all intents and purposes.

  In our unification algorithm, this is accomplished by just looking through our local definitions every time we check whether two expressions are definitionally equal.

  Outside of unification, the $\defequals$ symbol is used to indicate that we're defining a \insist{new} name, and the $\equals$ symbol is used for an equality about \insist{existing} expressions. To avoid confusion, every name we choose on the left of a $\defequals$ symbol should not be a name that already has a different meaning in the proof.



\section{Rules with contexts}


  We'll introduce the remaining rules of \acrshort{ipl} in later notes; for now, we'll quickly upgrade our old rules so that they allow us to prove things about \vocabs{entailment-judgement}.

  \begin{center}
    \TopIntro

    \AndIntro \quad \AndElimL \quad \AndElimR

    \OrIntroL \quad \OrIntroR

    \ImplElim
  \end{center}

  Each of these rules is identical to the way we wrote it in previous notes, but with the addition of $\metavar{\Gamma} \entails$ at the start of each \vocab{premise} and \vocab{conclusion}. Remember that the $\entails$ symbol is always the root of the \vocab{syntax tree}, so there are implicit parentheses to the right of it in these rules.

  What does this mean? Essentially, we're stating that none of these rules \insist{change} our context of assumptions in any way, reading bottom-to-top or top-to-bottom. Each rule works the same way as before, it just carries extra notation to indicate that the contexts must be the same both above and below the line.

  For example, to prove $\mono{P} \vee \mono{Q}$ in the context $\mono{Q} \cons \emptyset$, we can use \orIntroL\ along with a proof of $\mono{Q}$ in the \insist{same} context $\mono{Q} \cons \emptyset$. We cannot prove $\mono{P} \vee \mono{Q}$ in the context $\mono{Q} \cons \emptyset$ by proving $\mono{P}$ in a \insist{different} context like $\mono{P} \cons \emptyset$.

  \begin{center}
    \example{
      valid proof:

      \begin{center}
        \AxiomC{}
        \LeftLabel{\inAt{0}}
        \UnaryInfC{$\mono{Q} \in \mono{Q} \cons \emptyset$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\mono{Q} \cons \emptyset \entails \mono{Q}$}
        \LeftLabel{\orIntroR}
        \UnaryInfC{$\mono{Q} \cons \emptyset \entails \mono{P} \vee \mono{Q}$}
        \DisplayProof
      \end{center}
    }
    \example{
      \parbox{25em}{\color{red} invalid proof: the metavariable $\metavar{\Gamma}$ was substituted with $\mono{Q} \cons \emptyset$ in the conclusion of \orIntroL\ and substituted with $\mono{P} \cons \emptyset$ in the premise}

      \begin{center}
        \AxiomC{}
        \LeftLabel{\inAt{0}}
        \UnaryInfC{$\mono{P} \in \mono{P} \cons \emptyset$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\mono{P} \cons \emptyset \entails \mono{P}$}
        \LeftLabel{\color{red} \orIntroL}
        \UnaryInfC{$\mono{Q} \cons \emptyset \entails \mono{P} \vee \mono{Q}$}
        \DisplayProof
      \end{center}
    }
  \end{center}



\section{Subsumption}


  \subsection{Judgements}

    A \vocab{subsumption-judgement} between \vocabs{context} is another kind of \vocab{expression}, using the special \vocab{operator} \vocablink{sym-subsume}{$\position{\subsume}\position$}.

    We'll usually pronounce the $\subsume$ operator as ``is subsumed by'', or right-to-left as ``subsumes'': $\mono{C} \subsume \mono{D}$ is ``C is subsumed by D'' or ``D subsumes C''. More intuitively, we might say ``D includes every assumption in C''. By itself we'll call it the ``subsumption operator''.

    (It's a little more common to use a symbol like $\subset$ for this operator, but we want to avoid confusion with our $\subset$ operator in \vocabstyle{\acrshort{ipl}}.)


  \subsection{Rules}

    We have two \vocabs{rule-of-inference} for proving subsumption judgements:

    \begin{center} \SubNil \quad \SubCons \end{center}

    Intuitively, a proof of $C \subsume D$ for some contexts $C$ and $D$ shows that every proposition listed in $C$ is listed \insist{at least once} in $D$.

    Like with membership proofs, there may be multiple different proofs of the same subsumption judgement, specifically when the right context lists at least one proposition from the left context more than once.

    In addition to \subNil\ and \subCons, we add a new \vocab{derived-rule} for proving \vocabs{entailment-judgement}:

    \begin{center} \Rearrange \end{center}

    Intuitively, this says that if we can prove some proposition $P$ in some context $C$, then we can prove $P$ in \insist{any} context which subsumes $C$. We can think of a proof of $C \subsume D$ together with the \rearrange\ rule as a kind of ``conversion'' \insist{from} $C$ \insist{to} $D$.

    \rearrange\ is a subtle rule that we'll explore in depth in the next section. For now, let's just note that \rearrange\ allows us to take the step in constructing a proof tree that we considered in the introduction:

    \begin{prooftree}
        \AxiomC{}
      \LeftLabel{\subNil}
      \UnaryInfC{$\emptyset \subsume Q \cons R \cons \emptyset$}
      \AxiomC{$\emptyset \entails P$}
      \LeftLabel{\rearrange}
      \BinaryInfC{$Q \cons P \cons R \cons \emptyset \entails P$}
    \end{prooftree}

    Here are some examples of complete subsumption proofs:

    \begin{center}
      \example{
        \AxiomC{}
        \LeftLabel{\subNil}
        \UnaryInfC{$\emptyset \subsume \emptyset$}
        \DisplayProof
      }
      \example{
        \AxiomC{}
        \LeftLabel{\subNil}
        \UnaryInfC{$\emptyset \subsume \mono{P} \cons \emptyset$}
        \DisplayProof
      }
      \example{
        $\mono{C} \defequals \mono{Q} \cons \mono{P} \cons \emptyset$

        \bigskip

            \AxiomC{}
          \LeftLabel{\inAt{1}}
          \UnaryInfC{$\mono{P} \in \mono{C}$}
              \AxiomC{}
            \LeftLabel{\inAt{0}}
            \UnaryInfC{$\mono{Q} \in \mono{C}$}
              \AxiomC{}
            \RightLabel{\subNil}
            \UnaryInfC{$\emptyset \subsume \mono{C}$}
          \LeftLabel{\subCons}
          \BinaryInfC{$\mono{Q} \cons \emptyset \subsume \mono{C}$}
        \LeftLabel{\subCons}
        \BinaryInfC{$\mono{P} \cons \mono{Q} \cons \emptyset \subsume \mono{C}$}
        \DisplayProof
      }
      \example{
            \AxiomC{}
          \LeftLabel{\inAt{0}}
          \UnaryInfC{$\mono{P} \in \mono{P} \cons \emptyset$}
              \AxiomC{}
            \LeftLabel{\inAt{0}}
            \UnaryInfC{$\mono{P} \in \mono{P} \cons \emptyset$}
              \AxiomC{}
            \RightLabel{\subNil}
            \UnaryInfC{$\mono{P} \cons \emptyset \subsume \emptyset$}
          \LeftLabel{\subCons}
          \BinaryInfC{$\mono{P} \cons \emptyset \subsume \mono{P} \cons \emptyset$}
        \LeftLabel{\subCons}
        \BinaryInfC{$\mono{P} \cons \mono{P} \cons \emptyset \subsume \mono{P} \cons \emptyset$}
        \DisplayProof
      }
      \example{
            \AxiomC{}
          \LeftLabel{\inAt{1}}
          \UnaryInfC{$\mono{Q} \in \mono{P} \cons \mono{Q} \cons \emptyset$}
            \AxiomC{}
          \RightLabel{\subNil}
          \UnaryInfC{$\mono{P} \cons \mono{Q} \cons \emptyset \subsume \emptyset$}
        \LeftLabel{\subCons}
        \BinaryInfC{$\mono{Q} \cons \emptyset \subsume \mono{P} \cons \mono{Q} \cons \emptyset$}
        \DisplayProof
      }
      \example{
        $\mono{C} \defequals \mono{R} \cons \mono{Q} \cons \mono{P} \cons \mono{S} \cons \mono{P} \cons \emptyset$

        \bigskip

              \AxiomC{}
            \LeftLabel{\inAt{1}}
            \UnaryInfC{$\mono{Q} \in \mono{C}$}
                \AxiomC{}
              \LeftLabel{\inAt{2}}
              \UnaryInfC{$\mono{P} \in \mono{C}$}
                  \AxiomC{}
                \LeftLabel{\inAt{1}}
                \UnaryInfC{$\mono{Q} \in \mono{C}$}
                    \AxiomC{}
                  \LeftLabel{\inAt{0}}
                  \UnaryInfC{$\mono{R} \in \mono{C}$}
                    \AxiomC{}
                  \RightLabel{\subNil}
                  \UnaryInfC{$\emptyset \subsume \mono{C}$}
                \RightLabel{\subCons}
                \BinaryInfC{$\mono{R} \cons \emptyset \subsume \mono{C}$}
              \LeftLabel{\subCons}
              \BinaryInfC{$\mono{Q} \cons \mono{R} \cons \emptyset \subsume \mono{C}$}
            \LeftLabel{\subCons}
            \BinaryInfC{$\mono{P} \cons \mono{Q} \cons \mono{R} \cons \emptyset \subsume \mono{C}$}
          \LeftLabel{\subCons}
          \BinaryInfC{$\mono{Q} \cons \mono{P} \cons \mono{Q} \cons \mono{R} \cons \emptyset \subsume \mono{C}$}
        \DisplayProof
      }
    \end{center}


  \subsection{Properties}

    Eventually we'll be able to prove mathematical properties about operators like $\subsume$, but for now we'll just state some properties of $\subsume$ along with \vocabs{derived-rule} that we can use in our proofs.

    Note again that these properties are \insist{not} technically new \vocabs{rule-of-inference}: they're \insist{consequences} of the way that we've defined \subNil\ and \subCons\ (and \here\ and \there).


    \subsubsection{Shorthand}

      A subsumption proof corresponds to a \vocab{list} of membership proofs: if we have a proof of $C \subsume D$, we have exactly one membership proof for each proposition in $C$, which we can read left-to-right in the proof tree for $C \subsume D$ in the same order that the propositions are listed in $C$.

      Since each membership proof in a proof of $C \subsume D$ corresponds to an \vocab{index} in $D$, we can unambiguously express an entire subsumption proof as a list of indices with the same length as $C$. Each index indicates where in $D$ the corresponding proposition from $C$ is listed.

      We will use the \vocab{derived-rule} \subAt{i_0,i_1,\ldots,i_n} to represent the proof tree which corresponds to the list of indices $i_0, i_1, \ldots, i_n$ where $n$ is the length of the context on the left of the $\subsume$ sign in the conclusion of the rule. \subAt{}\ (with an empty list of indices) is the same as \subNil, and if we have \inAt{i_0} and \subAt{i_1,\ldots,i_n} we can produce \subAt{i_0,i_1,\ldots,i_n} by layering on another use of \subCons.

      For example:

      \begin{center}
        \example{
          \AxiomC{}
          \LeftLabel{\subAt{}}
          \UnaryInfC{$\emptyset \subsume \mono{P} \cons \mono{Q} \cons \emptyset$}
          \DisplayProof
        }
        \example{
          \AxiomC{}
          \LeftLabel{\subAt{1}}
          \UnaryInfC{$\mono{Q} \cons \emptyset \subsume \mono{P} \cons \mono{Q} \cons \emptyset$}
          \DisplayProof
        }
        \example{
          \AxiomC{}
          \LeftLabel{\subAt{1,0}}
          \UnaryInfC{$\mono{Q} \cons \mono{P} \cons \emptyset \subsume \mono{P} \cons \mono{Q} \cons \emptyset$}
          \DisplayProof
        }
        \example{
          \AxiomC{}
          \LeftLabel{\subAt{0,1,0}}
          \UnaryInfC{$\mono{P} \cons \mono{Q} \cons \mono{P} \cons \emptyset \subsume \mono{P} \cons \mono{Q} \cons \emptyset$}
          \DisplayProof
        }
        \example{
          \AxiomC{}
          \LeftLabel{\subAt{1,2,1,0}}
          \UnaryInfC{$\mono{Q} \cons \mono{P} \cons \mono{Q} \cons \mono{R} \cons \emptyset \subsume \mono{R} \cons \mono{Q} \cons \mono{P} \cons \mono{S} \cons \mono{P} \cons \emptyset$}
          \DisplayProof
        }
        \example{
          \AxiomC{}
          \LeftLabel{\subAt{1,4,1,0}}
          \UnaryInfC{$\mono{Q} \cons \mono{P} \cons \mono{Q} \cons \mono{R} \cons \emptyset \subsume \mono{R} \cons \mono{Q} \cons \mono{P} \cons \mono{S} \cons \mono{P} \cons \emptyset$}
          \DisplayProof
        }
      \end{center}

      Like with \inAt, to \vocab{check} a use of \subAt\ in a proof very carefully, we just rewrite it as the full version with \subNil\ and \subCons\ (and \here\ and \there) and then check the full version.


    \subsubsection{Reflexivity}

      For any well-formed context $C$, we can prove $C \subsume C$. This is the property of \vocab{reflexivity}: every context subsumes itself.

      Intuitively, a reflexive subsumption can always be proven with \subAt{0,1,\ldots,n-1} where $n$ is the length of the context.

      The derived rule for reflexivity is pointless to use with \rearrange, but it is provably valid:

      \begin{center} \SubRefl \end{center}

    \subsubsection{Transitivity}

      For any three well-formed contexts $C$, $D$, and $E$, if we have proofs of $C \subsume D$ and $D \subsume E$, then we can prove $C \subsume E$. This is the property of \vocab{transitivity}: we can ``chain'' subsumption proofs.

      It's a little trickier to see why this is true intuitively, but it helps to state a \vocab{lemma} (a small proof that we use to prove a larger proof): for any proposition \mono{P} and any contexts $C$ and $D$, if we have proofs of $P \in C$ and $C \subsume D$, then we can prove $P \in D$.

      Using this lemma, to construct a proof of $C \subsume E$, we take the proof of each proposition $P \in C$, use our $C \subsume D$ proof to prove $P \in D$, and then use our $D \subsume E$ proof to prove $P \in E$. This means that every proposition in $C$ is also in $E$, so we can conclude that $C \subsume E$.

      The derived rule for transitivity can be used to ``chain'' two subsumption proofs within a larger proof:

      \begin{center} \SubTrans \end{center}

    \subsubsection{Non-symmetry (and non-antisymmetry)}

      For two different well-formed contexts $C$ and $D$, if we have a proof of $C \subsume D$ then it \insist{might} be possible to prove $D \subsume C$, but it might not be possible. For example, we can prove both $\mono{P} \cons \emptyset \subsume \mono{P} \cons \mono{P} \cons \emptyset$ and $\mono{P} \cons \mono{P} \cons \emptyset \subsume \mono{P} \cons \emptyset$, and we can prove $\mono{P} \cons \emptyset \subsume \mono{P} \cons \mono{Q} \cons \emptyset$, but we cannot prove $\mono{P} \cons \mono{Q} \cons \emptyset \subsume \mono{P} \cons \emptyset$.

      If a proof of $C \subsume D$ always meant that we \insist{could} prove $D \subsume C$, then we would have the property of \vocab{symmetry}, like the standard meaning of $=$ in arithmetic.

      If a proof of $C \subsume D$ always meant that we \insist{could not} prove $D \subsume C$, then we would have the property of \vocab{antisymmetry}, like the standard meaning of $<$ in arithmetic.

      Since we can produce symmetric versions of some subsumption proofs but not others, our subsumption operator is neither symmetric nor antisymmetric. This is like the standard meaning of $\leq$ in arithmetic, but note that if $x \leq y$ and $y \leq x$ then $x = y$, and this is not true in general for $\subsume$: a counterexample from above is $\mono{P} \cons \emptyset$ and $\mono{P} \cons \mono{P} \cons \emptyset$, which are not \vocablink{definitional-equality}{definitionally equal} but do subsume each other.

      (In principle we could define an operator $\sqsubset$ to be an antisymmetric version of $\subsume$, like the relationship between $<$ and $\leq$, but we won't really have any use for a ``strict subsumption'' operator in our proofs. It might be a good exercise to think about how we'd define its rules, though!)



\section{Structural rules}

    In this section, we'll introduce three more important properties of our definition of subsumption, called \vocab{weakening}, \vocab{contraction}, and \vocab{exchange}. In our proof system, each of these properties will describe a \insist{special case} of what we can prove with \rearrange. For each of these principles, we'll give a corresponding \vocab{derived-rule} that expresses a common use of the principle. 

    When we have a proof system with all three of these properties, we say it is \vocab{structural}. In a structural proof system, when proving a \vocab{proposition}, we can ``rearrange'' our contexts of assumptions in flexible ways without affecting the validity of the proof.

    Like with the properties of \vocab{reflexivity} and \vocab{transitivity}, note that exchange, weakening, and contraction are \insist{not} new \vocabs{rule-of-inference}: they're \insist{consequences} of the way that we've defined \subNil\ and \subCons\ (and \here\ and \there). If we wanted a proof system without one or more structural principles (a \vocab{substructural} proof system), we'd have to modify our definitions of those rules.

    \longdemo{Why don't we want a substructural proof system right now? Why might we want a substructural proof system in other settings?}

    \subsection{Exchange}

      The structural principle of \vocab{exchange} says that $C \subsume D$ is provable whenever $C$ is a \vocab{permutation} of $D$, meaning they have the exact same elements but potentially in \insist{different orders}.

      In the context of \vocabstyle{\acrshort{ipl}}, the principle of exchange says that we are allowed to \insist{introduce} our assumptions in one order and then \insist{use} them in a different order. We might say that we use it to ``reorder'' our assumptions. (This is a little abstract for now, but we'll see it in practice soon.)

      A common use of exchange be expressed with this derived rule:

      \begin{center} \Exchange \end{center}

    \subsection{Weakening}

      The structural principle of \vocab{weakening} says that $C \subsume D$ is provable whenever $D$ has all of the propositions in $C$ and \insist{more}.

      In the context of \vocabstyle{\acrshort{ipl}}, the principle of weakening says that we are allowed to \insist{introduce} assumptions that we do not \insist{use}. We might say that we use it to ``ignore'' an assumption. (This is a right-to-left reading of the $\subsume$ operator, which is how we'll interpret it when we're reading proof trees top-down.)

      A common use of weakening be expressed with this derived rule:

      \begin{center} \Weaken \end{center}

    \subsection{Contraction}

      The structural principle of \vocab{contraction} says that $C \subsume D$ is provable whenever $D$ has each of the propositions of $C$ at least once, even if some elements from $C$ appear \insist{fewer} times in $D$ than in $C$.

      In the context of \vocabstyle{\acrshort{ipl}}, the principle of contraction says that we are allowed to \insist{introduce} an assumption once and then \insist{use} it multiple times. We might say that we use it to ``duplicate'' an assumption (again reading it right-to-left).

      A common use of contraction be expressed with this derived rule:

      \begin{center} \Contract \end{center}



\section{Satisfying assumptions}

  Intuitively, an \vocab{assumption} is a ``placeholder'' in a proof: it's a place in the proof tree where a ``real'' proof \insist{could} go if we had one, but we used the \assumed\ rule instead.

  For example, suppose we have some proof of the entailment $P \cons \emptyset \entails Q$, for some two propositions $P$ and $Q$. We'll represent this hypothetical proof using an informal notation, where the\ $\assumption{}$\ symbol stands for any arbitrary subtree.

  \begin{prooftree}
    \AxiomC{\assumption{}}
    \UnaryInfC{$P \cons \emptyset \entails Q$}
  \end{prooftree}

  Somewhere in this proof tree, there might be uses of the \assumed\ rule to prove $P$. For example, the tree might look something like this:

  \begin{prooftree}
            \AxiomC{}
          \LeftLabel{\inAt{0}}
          \UnaryInfC{$P \in P \cons \emptyset$}
        \LeftLabel{\assumed}
        \UnaryInfC{$P \cons \emptyset \entails P$}
      \UnaryInfC{$\assumption{}$}
              \AxiomC{}
            \LeftLabel{\inAt{1}}
            \UnaryInfC{$P \in R \cons P \cons \emptyset$}
          \LeftLabel{\assumed}
          \UnaryInfC{$R \cons P \cons \emptyset \entails P$}
        \UnaryInfC{$\assumption{}$}
          \AxiomC{}
        \UnaryInfC{$\assumption{}$}
      \BinaryInfC{$\assumption{}$}
    \BinaryInfC{$P \cons \emptyset \entails Q$}
  \end{prooftree}

  Now suppose we know that $P$ is a \vocab{tautology}: we have a separate proof of $\emptyset \entails P$.

  \begin{prooftree}
    \AxiomC{\assumption{}}
    \UnaryInfC{$\emptyset \entails P$}
  \end{prooftree}

  Intuitively, we should be able to ``plug in'' this proof everywhere that our first proof used the \assumed\ rule to prove $P$, replacing it with uses of \weaken\ and our second proof.

  \begin{prooftree}
            \AxiomC{\assumption{}}
          \UnaryInfC{$\emptyset \entails P$}
        \LeftLabel{\weaken}
        \UnaryInfC{$P \cons \emptyset \entails P$}
      \UnaryInfC{$\assumption{}$}
                \AxiomC{\assumption{}}
              \UnaryInfC{$\emptyset \entails P$}
            \LeftLabel{\weaken}
            \UnaryInfC{$P \cons \emptyset \entails P$}
          \LeftLabel{\weaken}
          \UnaryInfC{$R \cons P \cons \emptyset \entails P$}
        \UnaryInfC{$\assumption{}$}
          \AxiomC{}
        \UnaryInfC{$\assumption{}$}
      \BinaryInfC{$\assumption{}$}
    \BinaryInfC{$P \cons \emptyset \entails Q$}
  \end{prooftree}

  After this transformation, the proof has no uses of \assumed\ to prove $P$, so the proof should also be valid without assuming $P$ in the conclusion.

  \begin{prooftree}
            \AxiomC{\assumption{}}
          \UnaryInfC{$\emptyset \entails P$}
        \LeftLabel{\weaken}
        \UnaryInfC{$P \cons \emptyset \entails P$}
      \UnaryInfC{$\assumption{}$}
                \AxiomC{\assumption{}}
              \UnaryInfC{$\emptyset \entails P$}
            \LeftLabel{\weaken}
            \UnaryInfC{$P \cons \emptyset \entails P$}
          \LeftLabel{\weaken}
          \UnaryInfC{$R \cons P \cons \emptyset \entails P$}
        \UnaryInfC{$\assumption{}$}
          \AxiomC{}
        \UnaryInfC{$\assumption{}$}
      \BinaryInfC{$\assumption{}$}
    \BinaryInfC{$\emptyset \entails Q$}
  \end{prooftree}

  What did we just accomplish? We had a proof that $P$ is a tautology, and a proof that $Q$ is true when $P$ is true, and we ``combined'' them to prove that $Q$ is a tautology. We used the proof of $P$ to \vocab{satisfy} the assumption in our proof of $Q$.

  Like with our other derived rules, this reasoning can also be made formal in a more powerful proof system, but for now this intuition will be our justification for it. In our proof system, we'll represent this kind of ``plugging in'' with the \vocab{derived-rule} \satisfy:

  \begin{center} \Satisfy \end{center}

  The \satisfy\ rule allows us to ``remember'' proofs within our proof tree without writing them out more than once. Together with \vocabs{local-definition}, this can sometimes significantly cut down the size of our proof trees.

  \begin{center}
    \example{
      $\mono{P} \defequals \top \wedge \top$

      $\mono{C} \defequals \mono{P} \cons \emptyset$

      \bigskip

            \AxiomC{}
          \LeftLabel{\topIntro}
          \UnaryInfC{$\emptyset \entails \top$}
            \AxiomC{}
          \RightLabel{\topIntro}
          \UnaryInfC{$\emptyset \entails \top$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\emptyset \entails \mono{P}$}
              \AxiomC{}
            \LeftLabel{\inAt{0}}
            \UnaryInfC{$\mono{C} \entails \mono{P}$}
          \LeftLabel{\assumed}
          \UnaryInfC{$\mono{C} \entails \mono{P}$}
              \AxiomC{}
            \RightLabel{\inAt{0}}
            \UnaryInfC{$\mono{C} \entails \mono{P}$}
          \RightLabel{\assumed}
          \UnaryInfC{$\mono{C} \entails \mono{P}$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\mono{C} \entails \mono{P} \wedge \mono{P}$}
      \LeftLabel{\satisfy}
      \BinaryInfC{$\emptyset \entails \mono{P} \wedge \mono{P}$}
      \DisplayProof
    }
  \end{center}

  The \satisfy\ rule also offers some intuition about what exactly an \vocab{assumption} represents: an assumption is a ``placeholder'' where we might ``plug in'' a proof later, so a proof with assumptions can be thought of as a kind of ``tautology in waiting''. We might sometimes make assumptions that we can't prove, but \insist{if} we can \vocab{satisfy} all of the assumptions in a proof, then we've proven a tautology.



\input{include/footer.tex}
\end{document}
