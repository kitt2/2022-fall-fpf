\input{include/header.tex}

% auto-generated glossary terms
\input{notes/4.glt}

% rules of inference
\input{include/theories/context.tex}
\input{include/theories/ipl.tex}


\indextitle{Contexts and entailment}
\subtitle{Keeping track of our assumptions}

\makeglossaries


\begin{document}

\maketitle
\tableofcontents

\newpage


\section{Introduction}

  In our work with \vocabs{proof-tree} so far, we've been representing the \vocabs{assumption} in our proofs with a graphical notation, as in this example that we've previously seen:

  \longexample{
    proposition:
      \AxiomC{\assumption{\mono{P}}}
      \AxiomC{\assumption{\mono{Q}}}
      \BinaryInfC{$\mono{P} \wedge \left(\mono{Q} \wedge \top\right)$}
      \DisplayProof

    proof tree:
          \AxiomC{}
        \LeftLabel{\assumed}
        \UnaryInfC{\mono{P}}
            \AxiomC{}
          \LeftLabel{\assumed}
          \UnaryInfC{\mono{Q}}
            \AxiomC{}
          \RightLabel{\topIntro}
          \UnaryInfC{$\top$}
        \RightLabel{\andIntro}
        \BinaryInfC{$\mono{Q} \wedge \top$}
      \RightLabel{\andIntro}
      \BinaryInfC{$\mono{P} \wedge \left(\mono{Q} \wedge \top\right)$}
      \DisplayProof
  }

  With this notation, we list our assumptions when we state the \vocab{proposition} we're trying to prove, and then we can prove those assumptions in the \vocab{proof} by using the \assumed\ rule.

  This means the \assumed\ rule has a requirement that is not actually written in our proof tree itself: we are only allowed to use \assumed\ to conclude \mono{P} if we listed \mono{P} as an assumption in our \insist{problem statement}. The problem statement is outside of the \insist{proof} itself, so this means there is no ``witness'' inside the proof tree that shows we've followed this requirement.

  In this sense, our reasoning about assumptions so far has been ``external'' to our proof system: we can't write proofs \insist{about} assumptions, we can only write proofs \insist{using} assumptions. This is a subtle but important distinction: ideally, we want to be able to write proofs within our proof system about how assumptions \insist{work}.

  In these notes, we'll see how to ``internalize'' the requirement of the \assumed\ rule in our proof trees, so that each proof tree contains enough information to \vocab{check} each use of the \assumed\ rule without looking outside of the proof tree (or the \vocabs{rule-of-inference} that define our proof system). Building on this precise accounting of assumptions, later notes will introduce the rest of the rules of \vocabstyle{\acrfull{ipl}} that we haven't covered yet.



\section{Contexts}


  A \vocab{context} is a specific kind of \vocab{expression} in our proof system. There are two \vocabs{operator} that are specific to contexts:

  \begin{tabular}{l l}
    \vocablink{sym-emptyset}{\mono{$\emptyset$}} & is the \vocab{empty-context} (a constant) \\
    \vocablink{sym-cons}{\mono{$\position{\cons}\position$}} & is the \vocab{context-extension} operator
  \end{tabular}

  Note that $\position{\cons}\position$ is unrelated to the use of the colon symbol \AgdaSymbol{\position:\position} in Agda and in the \vocabs{typing-judgement} introduced in the next section; it's just a historical coincidence that they look similar. When talking about contexts, we traditionally pronounce $\emptyset$ as ``nil'' and pronounce $\position{\cons}\position$ as ``cons'', so $\mono{P} \cons \mono{Q} \cons \emptyset$ is ``P cons Q cons nil''.


  \subsection{Well-formed contexts}

    When we use a \vocab{context} in a proof tree, we will generally expect that the context is \vocablink{well-formed-context}{well-formed}. This is a restriction on the \insist{shape of the \vocab{syntax-tree}} that the context represents.

    We won't actually have to enforce this restriction explicitly, because our \vocabs{rule-of-inference} in the next section will ensure that any context used in a \vocab{complete-proof-tree} is a well-formed context. These section is designed to build an intuition for the structure of well-formed contexts.

    There are three ways to construct a \vocab{well-formed-context}:

    \begin{itemize}
      \item Any metavariable is a well-formed context.
      \item $\emptyset$ is a well-formed context.
      \item $e_1\ \cons\ e_2$ is a well-formed context if $e_1$ is a \vocab{proposition} and $e_2$ is another well-formed context.
    \end{itemize}

    These requirements ensure that the syntax tree of a well-formed context always ``leans to the \insist{right}'', which indicates that there is no branching structure in the context: if there is a $\position{\cons}\position$ node to the \insist{left} of another $\position{\cons}\position$ node, then the context is not well-formed. Propositions within a context may have branching structures, but a proposition cannot contain a context in \vocabstyle{\acrshort{ipl}}.

    Here are some example well-formed contexts using the operators of \acrshort{ipl} that we've seen:

    \begin{center}
      \example{
        context: $\top \cons \emptyset$

        tree:

          \begin{forest}
            [$\position{\cons}\position$
              [$\top$]
              [$\emptyset$]
            ]
          \end{forest}
      }
      \example{
        context: $\top \cons \left(\metavar{x} \cons \emptyset\right)$

        tree:

          \begin{forest}
            [$\position{\cons}\position$
              [$\top$]
              [$\position{\cons}\position$
                [$\metavar{x}$]
                [$\emptyset$]
              ]
            ]
          \end{forest}
      }
      \example{
        context: $\left(\metavar{a} \wedge \top\right) \cons \left(\left(\top \vee \metavar{b}\right) \cons \metavar{\Gamma}\right)$

        tree:

          \begin{forest}
            [$\position{\cons}\position$
              [$\wedge$
                [$\metavar{a}$]
                [$\top$]
              ]
              [$\position{\cons}\position$
                [$\vee$
                  [$\top$]
                  [$\metavar{b}$]
                ]
                [$\metavar{\Gamma}$]
              ]
            ]
          \end{forest}
      }
    \end{center}

    In practice, this means that every \vocab{well-formed-context} can be read as a \insist{\vocab{list} of propositions}, with $\position{\cons}\position$ as the ``separator'' and $\emptyset$ as a ``null-terminator''. (Note that a context can be ``terminated'' with a metavariable too, though.)

    Since well-formed contexts must have this list-like shape, it is not ambiguous to drop the parentheses around the $\position{\cons}\position$ operator when writing a well-formed context. We can also drop the parentheses around the propositions inside a context, because of the requirement that a proposition can't contain a context. (This means the $\position{\cons}\position$ operator comes later in the ``order of operations'' than all of our propositional operators.)

    Here are the examples from above with the optional parentheses left out:

    \begin{center}
      \example{
        well-formed context: $\top \cons \emptyset$

        tree:

          \begin{forest}
            [$\position{\cons}\position$
              [$\top$]
              [$\emptyset$]
            ]
          \end{forest}
      }
      \example{
        well-formed context: $\top \cons \metavar{x} \cons \emptyset$

        tree:

          \begin{forest}
            [$\position{\cons}\position$
              [$\top$]
              [$\position{\cons}\position$
                [$\metavar{x}$]
                [$\emptyset$]
              ]
            ]
          \end{forest}
      }
      \example{
        well-formed context: $\metavar{a} \wedge \top \cons \top \vee \metavar{b} \cons \metavar{\Gamma}$

        tree:

          \begin{forest}
            [$\position{\cons}\position$
              [$\position{\wedge}\position$
                [$\metavar{a}$]
                [$\top$]
              ]
              [$\position{\cons}\position$
                [$\position{\vee}\position$
                  [$\top$]
                  [$\metavar{b}$]
                ]
                [$\metavar{\Gamma}$]
              ]
            ]
          \end{forest}
      }
    \end{center}

    In these notes we'll often include the optional parentheses for clarity, but we'll sometimes drop them in very long examples that get hard to read with too many parentheses.



\section{Judgements}


  To represent \vocabs{assumption} with \vocabs{context} in our \vocabs{proof-tree}, we need \vocabs{rule-of-inference} that allow us to construct proofs about contexts.

  These rules and proofs can be defined independently from the rules of \vocabstyle{\acrshort{ipl}}: we'll have a little dedicated proof system just for reasoning about contexts, which is how we'll upgrade our \assumed\ rule. This will allow us to keep using the same \assumed\ rule even when we move away from \vocabstyle{\acrshort{ipl}}.

  In general, a \vocab{judgement} is similar to a \vocab{proposition}, in that it's an \vocab{expression} we might ask about the \vocab{truth} or \vocab{value} of. We will generally reserve the word \vocab{proposition} for expressions that only use the operators of \vocab{logic} (like $\wedge$ and $\vee$, unlike $\cons$ and $\emptyset$). A judgement may contain a proposition, but a proposition may not contain a judgement.

  We will define two kinds of judgements in these notes, and there will be a couple more to introduce as we go through the course material. From this point forward, the \vocabs{premise} and \vocab{conclusion} in our \vocabs{proof-tree} will be \vocabs{judgement} instead of just \vocabs{proposition}.

  This is where we start to see the ``inside'' and ``outside'' of our proof system: a judgement is a sort of ``meta-proposition''. Using judgements in our proof trees, we can write proofs about \vocabs{expression} in general, which allows us to be more precise about the \insist{structure} that we require in our proofs of propositions. (Remember: every proposition is an expression, but not every expression is a proposition.)

  From an informal perspective, the distinction between propositions and judgements is really just a technical distinction that ensures we don't end up allowing ``nonsense proofs'' in our proof system. You can intuitively think of propositions and judgements as different \vocabs{type} of things that we might want to prove, in the sense of the \vocab{type-system} of a programming language.


\section{Membership}


  \subsection{Judgements}

    A \vocab{membership-judgement} is a specific kind of \vocab{expression}: a \vocab{context} and a \vocab{proposition} separated by the special \vocab{operator} \vocablink{sym-in}{$\position{\in}\position$}. Whenever it appears an in expression, the $\in$ operator will be the root of the \vocab{syntax-tree} that the expression represents.

    We usually pronounce the $\in$ operator as ``is a member of'', or simply ``is in'': $\mono{P} \in \mono{C}$ is ``P is a member of C'' or ``P is in C''. By itself we'll call it the ``membership operator''.

    (The $\in$ symbol is based on the ``lunate'' epsilon $\epsilon$ in the ancient Greek alphabet, but it's rarely referred to that way in this setting. Note that it's different from the modern lowercase epsilon $\varepsilon$, which is like a small backwards 3.)


  \subsection{Rules}

    We have two \vocabs{rule-of-inference} for proving membership judgements:

    \begin{center} \Here \quad \There \end{center}

    Note that neither \here\ nor \there\ allows us to conclude that anything is a member of \vocab{sym-emptyset}, because $\emptyset$ can't \vocab{unify-with} the context in either conclusion. This is why we call it the \vocab{empty-context}.

    There is no branching in a proof of a membership judgement, since neither \here\ nor \there\ has more than one \vocab{premise}. Conveniently, the structure of a \vocab{complete-proof-tree} guarantees that the context to the right of the $\in$ is \vocablink{well-formed-context}{well-formed} at least up to the depth of the proof tree.

    \longdemo{Technically, our definitions of \here\ and \there\ allow us to prove a subsumption judgement where the right side of the $\subsume$ operator is not a \insist{completely} \vocab{well-formed-context}, like $\mono{P} \in \mono{P} \cons \mono{Q}$ (which is missing an $\emptyset$ at the end of the context). This is not something that we actually want to be able to do, but it's not really a problem for us in practice: why not?}

    A proof of a membership judgement is a proof that the proposition on the left of the $\in$ is listed in the context on the right of the $\in$. For example, if \mono{P}, \mono{Q}, and \mono{R} are all distinct \vocabs{constant}, we can prove $\mono{P} \in \mono{P} \cons \left(\mono{Q} \cons \emptyset\right)$ and $\mono{Q} \in \mono{P} \cons \left(\mono{Q} \cons \emptyset\right)$ but we cannot prove $\mono{R} \in \mono{P} \cons \left(\mono{Q} \cons \emptyset\right)$.

    More precisely, the \insist{depth} of a complete proof tree for a membership judgement can be seen as an \vocab{index} into the \vocab{list} that our context represents, reading the context from left to right (or reading its syntax tree from top to bottom). \here\ represents the index 0, and each use of \there\ adds 1 to an index. If the proof is correct, then the proposition to the left of the $\in$ is guaranteed to be at this index in the context.

    Here are some examples:

    \begin{center}
      \example{
        index: 0

        \bigskip

        tree:
          \AxiomC{}
          \LeftLabel{\here}
          \UnaryInfC{$\mono{P} \in \mono{P} \cons \emptyset$}
          \DisplayProof
      }
      \example{
        index: 0

        \bigskip

        tree:
          \AxiomC{}
          \LeftLabel{\here}
          \UnaryInfC{$\mono{P} \in \mono{P} \cons \left(\mono{P} \cons \emptyset\right)$}
          \DisplayProof
      }
      \example{
        index: 1

        \bigskip

        tree:
          \AxiomC{}
          \LeftLabel{\here}
          \UnaryInfC{$\mono{P} \in \mono{P} \cons \emptyset$}
          \LeftLabel{\there}
          \UnaryInfC{$\mono{P} \in \mono{P} \cons \left(\mono{P} \cons \emptyset\right)$}
          \DisplayProof
      }
      \example{
        index: 0

        \bigskip

        tree:
          \AxiomC{}
          \LeftLabel{\here}
          \UnaryInfC{$\mono{P} \in \mono{P} \cons \left(\mono{Q} \cons \emptyset\right)$}
          \DisplayProof
      }
      \example{
        index: 1

        \bigskip

        tree:
          \AxiomC{}
          \LeftLabel{\here}
          \UnaryInfC{$\mono{Q} \in \mono{Q} \cons \emptyset$}
          \LeftLabel{\there}
          \UnaryInfC{$\mono{Q} \in \mono{P} \cons \left(\mono{Q} \cons \emptyset\right)$}
          \DisplayProof
      }
      \example{
        index: 1

        \bigskip

        tree:
          \AxiomC{}
          \LeftLabel{\here}
          \UnaryInfC{$\left(\mono{P} \wedge \mono{Q}\right) \in \left(\mono{P} \wedge \mono{Q}\right) \cons \emptyset$}
          \LeftLabel{\there}
          \UnaryInfC{$\left(\mono{P} \wedge \mono{Q}\right) \in \mono{P} \cons \left(\mono{P} \wedge \mono{Q}\right) \cons \emptyset$}
          \DisplayProof
      }
      \example{
        index: 3

        \bigskip

        tree:
          \AxiomC{}
          \LeftLabel{\here}
          \UnaryInfC{$\mono{S} \in \mono{S} \cons \emptyset$}
          \LeftLabel{\there}
          \UnaryInfC{$\mono{S} \in \mono{R} \cons \mono{S} \cons \emptyset$}
          \LeftLabel{\there}
          \UnaryInfC{$\mono{S} \in \mono{Q} \cons \mono{R} \cons \mono{S} \cons \emptyset$}
          \LeftLabel{\there}
          \UnaryInfC{$\mono{S} \in \mono{P} \cons \mono{Q} \cons \mono{R} \cons \mono{S} \cons \emptyset$}
          \DisplayProof
      }
    \end{center}

    \longdemo{What does an \insist{incorrect} membership proof look like?}

    Note that there may be multiple \insist{different} proofs of the \insist{same} membership judgement, specifically when the context lists the proposition more than once.

    In later notes, we'll see why we might sometimes care about \insist{which} proof we have for a membership judgement, not just \insist{whether} a proof exists. For now, while we're working in \vocabstyle{\acrshort{ipl}}, we consider any correct proof to be ``as good'' as any other.


  \subsection{Shorthand}

    Since each proof of a membership judgement corresponds to a unique index, we can introduce a form of unambiguous shorthand for writing membership proofs quickly.

    We will use the \vocab{derived-rule} \inAt{n}\ to represent the proof tree which corresponds to the index $n$ for a given conclusion. You can think of this as a ``template'' for generating proofs: \inAt{0} is the same as \here, and if we have \inAt{i} we can produce \inAt{i+1} by layering on another use of \there.

    \begin{center}
      \example{
        with derived rule: \bigskip

          \AxiomC{}
          \LeftLabel{\inAt{0}}
          \UnaryInfC{$\metavar{a} \in \metavar{a} \cons \metavar{\Gamma}$}
          \DisplayProof

        \bigskip

        without derived rule: \bigskip

          \AxiomC{}
          \LeftLabel{\here}
          \UnaryInfC{$\metavar{a} \in \metavar{a} \cons \metavar{\Gamma}$}
          \DisplayProof
      }
      \example{
        with derived rule: \bigskip

          \AxiomC{}
          \LeftLabel{\inAt{1}}
          \UnaryInfC{$\metavar{b} \in \metavar{a} \cons \left(\metavar{b} \cons \metavar{\Gamma}\right)$}
          \DisplayProof

        \bigskip

        without derived rule: \bigskip

          \AxiomC{}
          \LeftLabel{\here}
          \UnaryInfC{$\metavar{b} \in \metavar{b} \cons \metavar{\Gamma}$}
          \LeftLabel{\there}
          \UnaryInfC{$\metavar{b} \in \metavar{a} \cons \left(\metavar{b} \cons \metavar{\Gamma}\right)$}
          \DisplayProof
      }
      \example{
        with derived rule: \bigskip

          \AxiomC{}
          \LeftLabel{\inAt{2}}
          \UnaryInfC{$\metavar{c} \in \metavar{a} \cons \left(\metavar{b} \cons \left(\metavar{c} \cons \metavar{\Gamma}\right)\right)$}
          \DisplayProof

        \bigskip

        without derived rule: \bigskip

          \AxiomC{}
          \LeftLabel{\here}
          \UnaryInfC{$\metavar{c} \in \metavar{c} \cons \metavar{\Gamma}$}
          \LeftLabel{\there}
          \UnaryInfC{$\metavar{c} \in \metavar{b} \cons \left(\metavar{c} \cons \metavar{\Gamma}\right)$}
          \LeftLabel{\there}
          \UnaryInfC{$\metavar{c} \in \metavar{a} \cons \left(\metavar{b} \cons \left(\metavar{c} \cons \metavar{\Gamma}\right)\right)$}
          \DisplayProof
      }
    \end{center}

    The term \vocab{derived-rule} indicates that the \inAt{} rule is \insist{not} technically a new \vocab{rule-of-inference} in our proof system: it's a ``convenience feature'' in our proof system, defined in terms of the rules we already have.

    By definition, any proof which uses derived rules can be converted into a proof that does not use derived rules. Intuitively, a derived rule cannot add any new ``power'' to our proof system. Their purpose is just to make proofs easier to write and reason about.

    This means it's never strictly necessary to use a derived rule in order to prove something, even though it can often make the proof much more convenient to write.

    When we're constructing an individual proof tree, derived rules behave just like inference rules: we can use them anywhere that the corresponding unification problems are solvable.

    We'll be able to make this kind of reasoning fully formal once we introduce \vocabs{natural-number} and \vocab{proof-relevance} into our proof system. For now, the intuitive justification given above is our ``reasoning about the general structure of proof trees''.

    To \vocab{check} a use of \inAt{} in a proof very carefully, we can just rewrite it as the full version with \here\ and \there\ and then check the full version.



\section{Entailment}


  With the $\position{\in}\position$ operator and the \here\ and \there\ rules, we can upgrade the definition of our \assumed\ rule to include a \insist{justification} for each use of the rule. This allows us to \vocab{check} a proof that uses the \assumed\ rule without looking outside of the proof itself and the \vocabs{rule-of-inference} that define the proof system.


  \subsection{Judgements}

    An \vocab{entailment-judgement} is another kind of \vocab{expression}, using the special operator \vocablink{sym-entails}{$\position{\entails}\position$}. In the rules that define entailment, there will be a \vocab{context} on the left side of the $\entails$ and a \vocab{proposition} on the right side. Whenever it appears an in expression, the $\entails$ operator will be the root of the \vocab{syntax-tree} that the expression represents.

    We usually pronounce the $\entails$ operator as ``entails'': $\mono{C} \entails \mono{P}$ is ``C entails P''. More intuitively, we might say ``if we have proofs for everything in the list C, then we can prove P''. By itself, we sometimes call $\entails$ the \vocab{turnstile} symbol. (If you're not familiar with the term ``turnstile'', search for images of a subway turnstile: the symbol is called a turnstile because it looks like a classic subway turnstile viewed from the side.)


  \subsection{Rules}

    When we come back to \vocabstyle{\acrshort{ipl}} in later notes, we'll modify all of our \vocabs{rule-of-inference} to use entailment judgements in their premises and conclusions. For now, we'll just see how the \assumed\ rule can be defined more precisely using an entailment judgement in its conclusion.

    \begin{prooftree}
      \AxiomC{$\metavar{a} \in \metavar{\Gamma}$}
      \LeftLabel{\assumed}
      \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
    \end{prooftree}

    This rule essentially says that a proof of a \vocab{membership-judgement} can be ``converted to'' a proof of an \vocab{entailment-judgement}. Reading bottom-up, it says that if we want to prove entailment, \insist{one} way to do that is to prove membership.

    The $\entails$ operator is different from the $\in$ operator because \assumed\ is not the \insist{only} way to prove an entailment judgement: every membership proof can be ``converted to'' an entailment proof, but not all entailment proofs can be ``converted back'' to membership proofs. A proof system like \acrshort{ipl} will define its own additional rules for proving entailment, which in general will have different structures than this \assumed\ rule.

    Our entailment proofs won't get interesting until we introduce these additional rules, but we can write very minimal entailment proofs already:

    \begin{center}
      \example{
        \begin{prooftree}
          \AxiomC{}
          \LeftLabel{\inAt{1}}
          \UnaryInfC{$\mono{Q} \in \mono{P} \cons \mono{Q} \cons \emptyset$}
          \LeftLabel{\assumed}
          \UnaryInfC{$\mono{P} \cons \mono{Q} \cons \emptyset \entails \mono{Q}$}
        \end{prooftree}
      }
    \end{center}

    We could read this proof as saying ``the statement ``if I have proofs for everything in the list [P, Q] then I can prove Q'' is true because Q is in the list [P, Q] at index 1.''

    Note how this is not just a proof \insist{using} assumptions, but a proof \insist{about} assumptions: the thing that we proved is a statement about how our definition of an ``assumption'' works. This is what will allow us to define the rest of the rules of \acrshort{ipl}.


  \subsection{Tautologies}

    If we can prove $\emptyset \entails P$ for some \vocab{proposition} $P$, then we say that $P$ itself is a \vocab{tautology}: a proposition that can be proven with no assumptions at all. This is most often the end goal of a proving effort.

    (Note that in an informal sense, every proof depends on ``assumptions'' that are encoded in the rules of the \vocab{proof-system}, so every proof is a proof with some ``assumptions''. When we say that a tautology is a proof with no assumptions, we're specifically focusing on the \vocab{context} of assumptions in an \vocab{entailment-judgement}.)

    For a minimal example, let's take a peek at the definition of our upgraded \topIntro\ rule:

    \begin{prooftree}
      \AxiomC{}
      \LeftLabel{\topIntro}
      \UnaryInfC{$\metavar{\Gamma} \entails \top$}
    \end{prooftree}

    This rule says that we can always prove $\top$ in \insist{any} context of assumptions: $\top$ is the proposition that is always true regardless of anything else.

    As a straightforward consequence of this rule, because $\metavar{\Gamma}$ unifies with $\emptyset$, we can show that $\top$ is a tautology:

    \begin{prooftree}
      \AxiomC{}
      \LeftLabel{\topIntro}
      \UnaryInfC{$\emptyset \entails \top$}
    \end{prooftree}

    In general, a proof of a tautology actually \insist{may} use the \assumed\ rule. This might sound surprising at first, but we'll have ways to introduce \vocab{local} assumptions that are only usable in some parts of the proof tree.

    To prove a tautology, the \vocab{conclusion} at the very bottom of the proof tree must have an empty context ($\emptyset$), but propositions elsewhere in the proof tree may have other contexts. This means that as a special case, the \insist{bottom-most} rule in the proof for a tautology cannot be \assumed, because we have no way to prove $P \in \emptyset$ for any $P$.



\input{include/footer.tex}
\end{document}
