{-
As we've discussed in lecture, I'm unfortunately too short on time to produce
nice PDF lecture notes at this point in the quarter, so I'll be putting together
commented Agda notes for the next couple weeks.

This file reviews the Agda material that we discussed in Week 7, along with a
bit of extra material on falsity and negation in STLC/Agda.
-}
module notes11 where

-- First, here are the type definitions that we covered in lecture.

-- The Unit type corresponds to the ⊤ proposition in IPL.
-- The unit constructor corresponds to the ⊤-Intro axiom.
data Unit : Set where -- "Unit is a type"
  unit : Unit -- "unit is a zero-argument constructor for Unit"
              -- also, "unit has type Unit"

-- The _×_ type corresponds to the _∧_ operator in IPL.
-- The _,_ constructor corresponds to the ∧-Intro rule.
data _×_ (A B : Set) : Set where -- "for *any* types A and B, A × B is a type"
  _,_ : A → B → A × B -- "_,_ is a two-argument constructor for _×_"
                      -- also, "if x has type A and y has type B,
                      --        then (x , y) has type A × B"

-- Note that the _,_ operator always requires spaces on *both* sides in Agda!
-- For example, "(x, y)" is incorrect, "(x , y)" is correct.

-- The _⊎_ type corresponds to the _∨_ operator in IPL.
-- The inj₁ and inj₂ constructors correspond to the ∨-Introᴸ and ∨-Introᴿ rules.
data _⊎_ (A B : Set) : Set where -- "for *any* types A and B, A × B is a type"
  inj₁ : A → A ⊎ B -- "if x has type A, then inj₁ x has type A ⊎ B"
  inj₂ : B → A ⊎ B -- "if x has type B, then inj₂ x has type A ⊎ B"

-- Agda's built-in _→_ function type corresponds to the _⊂_ operator in IPL.
-- This is special, because it's built-in. *Naming* an argument on the left side
-- of a function definition corresponds to the ⊂-Intro rule.

-- Every function in Agda has exactly one argument and exactly one return value.
-- For any two types A and B, A → B is the type of functions with an argument of
-- type A and a return value of type B.


-- We introduce P, Q, R, and S as arbitrary types: we assume that they exist,
-- and that they are types, and that there *might* be values of these types, but
-- that's all that we assume about them. This "variable" keyword is actually a
-- form of shorthand in Agda: we'll see what it expands to soon, when we discuss
-- quantifiers.
variable
  P Q R S : Set

-- This proof corresponds to ⊂-Intro (Assumed ∈-Here). The ⊂-Intro rule comes
-- from *naming* the argument on the left side of the = symbol. This definition
-- is a function which takes an argument of type P, names it x, and then returns
-- the argument by name.
→-reflexive : P → P
→-reflexive x = x


-- We define types by giving their constructors (introduction rules) as above,
-- and then Agda gives us their eliminators (elimination rules) "for free". We
-- can access an eliminator by pattern matching, which means giving *multiple*
-- definitions for a function.

-- For example, to "ask" whether a proof of P ⊎ Q contains a proof of P or a
-- proof of Q, we can give *separate* definitions: one for when the input is a
-- proof of P, and one for when the input is a proof of Q.
⊎-commutative : (P ⊎ Q) → (Q ⊎ P)
⊎-commutative (inj₁ x) = inj₂ x -- in this definition, x : P
⊎-commutative (inj₂ y) = inj₁ y -- in this definition, y : Q

-- Note that each definition is an entirely separate scope! In the proof above,
-- x is *only* usable in the first definition.

-- The Agda plugin will intelligently pattern-match for you. In the hole below,
-- enter x and then press C-c C-c to "case split" on x. Notice how your proof
-- updates with one case for each possible constructor of x.
⊎-associative : (P ⊎ (Q ⊎ R)) → ((P ⊎ Q) ⊎ R)
⊎-associative x = ?


-- Pattern matching on a value of the _⊎_ type corresponds to a use of the
-- ∨-Elim rule in IPL. We can derive the ∧-Elimᴸ and ∧-Elimᴿ rules in a similar
-- way: since there is only one constructor of the _×_ type, there is only one
-- "case" in each pattern match.
proj₁ : (P × Q) → P
proj₁ (x , y) = x

proj₂ : (P × Q) → Q
proj₂ (x , y) = y


-- The ⊂-Elim rule is special, because the _→_ type is special built-in magic.
-- When we want to eliminate an implication, we use an "invisible" operator
-- which is written with whitespace.
→-Elim : ((P → Q) × P) → Q
→-Elim (f , x) =
  f x -- this space between "f" and "x" is an operator

-- More precisely, the syntax tree of "f x" looks like this:
--   space   <---- this node is the ⊂-Elim
--    / \
--   f   x

-- This "invisible" space operator also corresponds to *calling* the function
-- with an argument: "f x" is the function "f" called with the argument "x".


-- The Void type corresponds to the ⊥ proposition in IPL.
-- There are *no* constructors for Void, because there is no introduction rule
-- for ⊥.
data Void : Set where -- this single line is the entire definition of Void

-- Note that this is not the same "void" as in C++ and similar languages!

-- Agda has a very special way to work with "empty" types like Void, since it's
-- able to recognize that there are no constructors of Void.

-- When defining a function, if the argument type is Void, we are allowed to
-- *skip* giving a return value. To indicate this, we use the special notation
-- () in place of an argument name.
absurd : Void → P
absurd () -- this is valid because the argument has no possible constructors

-- Note that this is a very different use of "()" than in Haskell: Haskell's use
-- of the notation "()" corresponds to our Unit type in Agda. The () notation in
-- Agda is not an expression form: it can *only* be used in pattern matching.

-- The () notation in Agda corresponds to the ⊥-Elim rule in IPL. Intuitively,
-- we're saying "this is dead code, so I don't have to produce a return value".

-- Note that Agda *is* checking our work here: if you uncomment the definition
-- of bad-Unit-Elim below, it will *not* typecheck, because the argument has at
-- least one possible constructor.
-- bad-Unit-Elim : Unit → P
-- bad-Unit-Elim ()


-- Like in IPL, we can define negation in terms of implication and falsity.
-- In Agda, we can do this very straightforwardly by writing a function from
-- types to types.
¬_ : Set → Set
¬ A = A → Void

-- Now whenever we write "¬ P" for any type P, Agda will interpret it as if we
-- had written "P → Void".

-- Try to walk through this proof step-by-step with the Agda plugin to see how
-- it works!
¬¬-Intro : P → ¬ ¬ P
¬¬-Intro p np = np p
