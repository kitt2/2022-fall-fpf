\input{include/header.tex}

% auto-generated glossary terms
\input{notes/9.glt}

% rules of inference
\input{include/theories/iplc.tex}
\input{include/theories/bool.tex}


\indextitle{Classical propositional logic}
\subtitle{Suspending our disbelief}

\makeglossaries


\begin{document}

\maketitle
\tableofcontents

\newpage


\section{Introduction}


  In the notes on \vocab{negation}, we showed that \vocabstyle{\acrfull{ipl}} has a principle of ``\vocab{double-negation} introduction'', but does \insist{not} have a principle of ``\vocab{double-negation} elimination''.

  We gave some intuition for this fact in terms of ``real-world knowledge'': it is possible to know that something is ``not disprovable'' without actually knowing a proof of it. As we'll see soon, \acrshort{ipl}'s focus on ``real-world knowledge'' gives it a special connection to \vocab{lambda-calculus}, which makes it very relevant to the study of \vocabs{programming-language} and \vocab{computation}.

  But \acrshort{ipl} is just one system of logic, and its specific idea of ``real-world knowledge'' is just one thing that we might want to reason about logically. There are many other systems of logic whose proofs are useful, just in different domains and applications.

  In particular, it would be irresponsible to ignore \vocabstyle{\acrfull{cpl}}, also known as \vocab{Boolean-logic} or \vocab{Aristotelian-logic}. \acrshort{cpl} is the logic of \mono{boolean} data types in programming languages.

  Most programming work involves much more logical structure than simple \mono{boolean} data types, so \acrshort{cpl} turns out to be a somewhat limiting perspective on \vocab{programming}. More usefully, \acrshort{cpl} can be seen as a logic of idealized \vocabs{digital-circuit}, or simply as a logic of \vocabs{lookup-table}.

  The formal relationship between \acrshort{ipl} and \acrshort{cpl} is deep and usually surprising the first time you encounter it. In these notes, we'll see that our familiar Boolean logic has a very revealing interpretation in terms of \vocabs{intuitionistic-proof}.



\section{Independence}


  As we've seen, the judgement $\emptyset \entails \negate \negate \mono{P} \subset \mono{P}$ is \vocab{independent-of} \acrshort{ipl}: it is neither \vocab{intuitionistically-provable} nor \vocab{intuitionistically-disprovable}.

  We've also seen that $\negate \negate \mono{P} \subset \mono{P}$ is a \vocab{classical-tautology}, meaning that it \insist{is} possible to prove $\emptyset \entails \negate \negate \left(\negate \negate \mono{P} \subset \mono{P}\right)$. In this sense, we have that $\negate \negate \mono{P} \subset \mono{P}$ is intuitionistically ``not false''.

  In fact, there are many other propositions like $\negate \negate \mono{P} \subset \mono{P}$: propositions that are classical tautologies but are independent of \acrshort{ipl}. Here are a couple notable ones:

  \begin{itemize}
    \item $\mono{P} \vee \negate \mono{P}$
    \item $\negate \left(\mono{P} \wedge \mono{Q}\right) \subset \left(\negate \mono{P} \vee \negate \mono{Q}\right)$
    \item $\left(\mono{P} \subset \mono{Q}\right) \subset \left(\negate \mono{P} \vee \mono{Q}\right)$
  \end{itemize}

  Each of these propositions is intuitionistically ``not false'', so each one is classically true, but none of them can be proven intuitionstically true.

  \longdemo{Interestingly, if we assume any \insist{one} of the propositions listed above, we can prove \insist{all} of the others in \acrshort{ipl}.}

  
  \subsection{The excluded middle}

    One of the propositions from above might jump out at you: $\mono{P} \vee \negate \mono{P}$. This is traditionally known as \vocabstyle{the \acrfull{lem}}.

    Informally, we might read \acrshort{lem} as ``$\mono{P}$ must be either true or false''. Since \mono{P} is an \vocab{arbitrary-constant}, we might also read \acrshort{lem} as ``\insist{any} proposition must be either true or false''.

    Remember that \acrshort{ipl}'s interpretation of ``truth'' is ``provability'', and more precisely we might say something is \vocab{intuitionistically-true} when it's ``known to be proven'' and \vocab{intuitionistically-false} when it's ``known to be disproven''.

    With this focus on ``real-world knowledge'', it's clear that \acrshort{lem} is not \vocab{intuitionistically-true} for \insist{all} propositions which we might substitute for \mono{P}, because there are still many unsolved problems in math and logic to date. The Collatz conjecture is an example of a proposition that is neither ``known to be proven'' nor ``known to be disproven''.

    In fact, there are \insist{four} possible ``real-world'' outcomes for the effort to prove the Collatz conjecture in any particular proof system:

    \begin{itemize}
      \item It might be proven true in that proof system.
      \item It might be proven false in that proof system.
      \item It might be proven to be independent of that proof system.
      \item It might just never be settled in any way at all.
    \end{itemize}

    Okay, fine, but\ldots\ something probably feels off to you here. Intuitively, something probably feels ``true'' about \acrshort{lem} in some way, even if it's not specifically \vocab{intuitionistically-true}.

    You might reasonably feel like you've never encountered anything in the ``real world'' that seemed to be ``neither true nor false'', in a basic intuitive sense about what is and is not \insist{possible} to know.

    Surely the Collatz conjecture has some resolution in principle, even if humanity never discovers it? Could there really be any \insist{problem} with assuming that it ``must be either true or false'', even in \acrshort{ipl}?

    Well, no, there is no ``problem'': \acrshort{lem} is \vocab{independent-of} \acrshort{ipl}, so it is consistent to \vocab{assume} either $\mono{P} \vee \negate \mono{P}$ or $\negate \left(\mono{P} \vee \negate \mono{P}\right)$ as long as we don't assume them both at the same time. If we assume $\mono{P} \vee \negate \mono{P}$, then \insist{every} proposition is ``either true or false'', so the Collatz conjecture certainly is.

    In fact, we know that assuming \acrshort{lem} can't ``break'' our proof system, because we can prove $\negate \negate \left(\mono{P} \vee \negate \mono{P}\right)$. If we assume \acrshort{lem}, there is no possibility that we might actually \vocab{disprove} \acrshort{lem} later, which would be a problem if it was possible.

    What if we encode \acrshort{lem} directly into our \vocabs{rule-of-inference}?



\section{Classical propositional logic}


  There are actually many different equivalent ways to define \vocabstyle{\acrfull{cpl}}. For our definition, we'll build on the intuition for \vocabstyle{the \acrfull{lem}} from the previous section.


  \subsection{Rules}

    We add a single \vocab{axiom} to the rules of \acrshort{ipl}:

    \begin{center}
      \LEM
    \end{center}

    Now we have our definition of \acrshort{cpl}. That's it!


  \subsection{Properties}

    Note that \lem\ is an \vocab{inference-rule}, \insist{not} a \vocab{derived-rule}. \insist{Not} every proof that uses \lem\ can be rewritten to avoid using \lem.

    We'll state one major property of \acrshort{cpl} here, and then come back to examine it in more depth later:

    \begin{itemize}
      \item For any proposition $P$, $P$ is a \vocab{tautology} in \acrshort{cpl} if and only if $\negate \negate P$ is a \vocab{tautology} in \acrshort{ipl}.
    \end{itemize}

    This means that if $P$ can be proven with the \acrshort{lem} axiom then $\negate \negate P$ can be proven without the \acrshort{lem} axiom, and vice versa. This is sometimes known as the ``\vocab{double-negation} translation'' of \acrshort{cpl} into \acrshort{ipl}.


  \subsection{Intuition}

    With one seemingly innocent axiom, the entire meaning of our proof system changes. We are no longer writing proofs about ``actual present knowledge''; we're writing proofs about ``potential knowledge'', under the intuitive assumption that ``every logical problem must have a resolution even if nobody ever discovers it''.

    In one sense, we gain a lot of power by adding \acrshort{lem} as an axiom: we can prove many things that we couldn't prove before, and our proof system actually becomes much more ``predictable'' in a certain specific way. Our proofs are still useful, even though their meaning has changed.

    In a different sense, we \insist{lose} a lot of power by adding \acrshort{lem} as an axiom: the connection to \vocab{lambda-calculus} and \vocab{computation} changes in a way that makes it less theoretically flexible. We'll see what this means in detail soon.

    For now, note that \acrshort{lem} is neither an \vocab{introduction-rule} nor an \vocab{elimination-rule}. There is no \vocab{rewrite-rule} associated with \acrshort{lem}: if we use \acrshort{lem} to prove the left premise of \orElim, it is \insist{not} a \vocab{detour}, unlike with \orIntroL\ and \orIntroR. We've lost the guarantee that every \vocab{inference-rule} which defines an operator has a \vocab{rewrite-rule} associated with it.

    In general, \acrshort{ipl} and \acrshort{cpl} are both useful, but for somewhat different purposes. Classical logic is much more common in ``traditional'' mathematics, but intuitionistic and classical logic are \insist{both} very widely used in modern computer science.

    In particular, we'll see soon how intuitionistic logic is the basis of \vocab{type-theory}, which is a ubiquitous tool for analyzing the behavior of programs. Classical logic has some relation to type theory, but it's much less broadly useful in that domain.

    As we'll see below, classical logic mostly finds application in computer science within the domain of \vocab{Boolean-logic}. Boolean logic is a critical component of modern computer architecture and is relevant to most modern programs, but most modern programs also involve ``higher-level'' forms of logic with other data types.


  \subsection{Proof by contradiction}

    One core technique of classical logic is traditionally called \vocab{proof-by-contradiction}. In a certain sense, proof by contradiction is the most ``primitive'' technique that is available in classical logic but not intuitionistic logic.

    The principle of \vocab{proof-by-contradiction} says that ``if it is contradictory to assume that something is false, then it must be true''. This is one way to read the ``\vocab{double-negation} elimination'' proposition $\negate \negate \mono{P} \subset \mono{P}$.

    Formally, proof by contradiction is justified by the fact that $\negate \negate \mono{P} \subset \mono{P}$ is a \vocab{classical-tautology}. Informally, the idea is justified by $\lem$ and by our definition of \vocab{negation}.

    Remember that $\negate P \defequals P \subset \bot$, so $\negate \negate P$ can be read as $\negate P \subset \bot$, or ``if $P$ is false then $\bot$ is true''. $\bot$ can only be true in an \vocab{inconsistent} context, so we can also read this as ``if $P$ is false in some context, then that context must be inconsistent with $P$''.

    More formally, if $C \entails \negate P \subset \bot$ is provable, then $\negate P \cons C$ is an inconsistent context. If we set $C \defequals \emptyset$ in this statement, we can read $\emptyset \entails \negate P \subset \bot$ as saying ``if $P$ is a \vocab{contradiction}, then $\negate P \cons \emptyset$ is an inconsistent context''.

    The only way $\negate P \cons \emptyset$ can be inconsistent is if $\negate P$ is \vocab{false} or if $P$ is \vocab{true}. In \acrshort{ipl}, these are \insist{different} conditions: knowing $\negate P$ is \vocab{intuitionistically-false} provides \insist{no guarantee} that $P$ is \vocab{intuitionistically-true}. In \acrshort{cpl}, they are the \insist{same} condition: if $\negate P$ is \vocab{classically-false}, then $P$ \insist{must} be \vocab{classically-true}.

    All of this shows that \vocab{proof-by-contradiction} ``works'' in a classical setting: if we assume that $P$ is false and then prove $\bot$, that ``counts'' as a proof that $P$ is true. Intuitively, proving $\bot$ really means that we're proving the context is inconsistent.

    Proof by contradiction is very common in ``traditional'' math proofs. It's often used informally without being explicitly mentioned: an informal proof might have a structure that reads like ``Assume $P$ is false\ldots (proofs about other related things)\ldots but this is a contradiction, so $P$ must be true''. This proof structure is the essence of \vocab{proof-by-contradiction} in informal proofs.



\section{Boolean logic}


  The terms \vocabstyle{\acrlong{cpl}} and \vocab{Boolean-logic} both refer to the same system of logic, but they usually indicate different perspectives.

  When we talk about \acrshort{cpl}, we're usually talking about \vocabs{proof}. When we talk about \vocab{Boolean-logic}, we're usually talking about a system of \vocab{calculation} which happens to ``agree'' with the proof system that we call \acrshort{cpl}.

  You've probably encountered Boolean logic and its system of calculation before, so some of this material will probably be very familiar to you. Don't lose focus, though! You'll probably be surprised by the connection between Boolean logic and our definition of \acrshort{cpl}.


  \subsection{Values}

    In \vocab{Boolean-logic}, we reason about two \vocabs{Boolean-value} (or \vocabs{truth-value}), which we will write as the \vocabs{constant} \yes\ and \no. These are effectively pieces of \vocab{data}: we might think of \yes\ and \no\ as the ``on'' and ``off'' positions of a binary switch, just like how data is physically stored on a modern computer.

    To be very clear, we'll always write \yes\ or \no\ in these notes when we're talking about the \vocabs{Boolean-value}. It is traditional to call these values ``true'' and ``false'', but they are \insist{different} than our use of the words \vocab{true} and \vocab{false} in \vocab{proof-theory}!

    \begin{itemize}
      \item The words \vocab{true} and \vocab{false} are adjectives that describe properties of propositions.
      \item The values \yes\ and \no\ are pieces of data that could be stored on a computer.
    \end{itemize}

    There is a connection between the \vocab{value} \yes\ and the property of a proposition being \vocab{true}, but they are \insist{not the same thing}.

    The two values \yes\ and \no\ are the \insist{only} Boolean values. If we say something like ``$X$ is a Boolean value'', then by definition $X$ must be either \yes\ or \no.


  \subsection{Propositions}

    A \vocab{Boolean-proposition} is a \vocab{expression} that may involve these \vocabs{operator}:
    \begin{itemize}
      \item \yes
      \item \no
      \item $\position{\wedge}\position$
      \item $\position{\vee}\position$
      \item $\position{\subset}\position$
      \item ${\negate}\position$
    \end{itemize}

    Boolean propositions are very similar to our propositions in \acrshort{ipl}, but we've replaced the constants $\top$ and $\bot$ with the constants \yes\ and \no\ in order to make it clear that we're not talking about the same logic anymore.

    A Boolean proposition may also include any number of \vocabs{metavariable}, written as we've seen before (like \metavar{a} or \metavar{x}). It is more traditional to call these ``variables'' in Boolean logic, but we'll call them \vocabs{metavariable} to avoid confusing them with Agda's \vocabs{variable}, which are not the same as in Boolean logic.

    Like in \acrshort{ipl}, we will say that a Boolean expression is \vocab{closed} if it has no metavariables, and \vocab{open} if it has metavariables.


  \subsection{Lookup tables}

    To define the operators of Boolean logic, we use a special kind of ``spreadsheet'' called a \vocab{lookup-table} (or \vocab{truth-table}).

    A lookup table defines a \vocab{function} over Boolean values (or other finite sets). Since we know that \yes\ and \no\ are the \insist{only} possible Boolean values, we can define a function with a single Boolean input just by specifying the output of the function for both possible inputs.

    For example, here is the lookup table that defines the $\negate$ operator in \vocab{Boolean-logic}.

    \begin{center}
      \NOT
    \end{center}

    In the lookup tables in these notes, the columns to the left of the \insist{thick} vertical line are the \insist{inputs} to the function, and the column to the right of the thick vertical line is the \insist{output} of the function.

    The vertical order of rows in the table is not meaningful: this lookup table below has the same as the lookup table for $\negate$ above.

    \begin{center}
      \begin{tabular}{c ? c}
        $\metavar{a}$ & $\negate \metavar{a}$ \\
        \thickhline
        \no & \yes \\
        \hline
        \yes & \no
      \end{tabular}
    \end{center}

    The lookup table that defines the $\negate$ operator tells us that $\negate \yes \defequals \no$ and $\negate \no \defequals \yes$.

    Our Boolean lookup tables may have \insist{multiple inputs}, but only \insist{one output}. For example, here is the lookup table that defines the $\wedge$ operator in \vocab{Boolean-logic}.

    \begin{center}
      \AND
    \end{center}

    This lookup table tells us that $\yes \wedge \yes \defequals \yes$, $\yes \wedge \no \defequals \no$, $\no \wedge \yes \defequals \no$, and \mbox{$\no \wedge \no \defequals \no$}.


  \subsection{Operator definitions}

    Here are the lookup tables that define each of our \acrshort{cpl} \vocabs{operator} in Boolean logic.

    \begin{center}
      \NOT \quad \AND \quad \OR \quad \IMPLIES
    \end{center}


  \subsection{Counting lookup tables}

    There are a finite number of different lookup tables for any given number of inputs. Specifically, there are $4^n$ (or more suggestively $2^{2n}$) different Boolean lookup tables with $n$ inputs, for any non-negative integer $n$.

    There are $4^1 = 4$ different lookup tables with 1 input:

    \begin{center}
      \begin{tabular}{c ? c}
        \textbf{Input} & \textbf{Output} \\
        \thickhline
        \yes & \yes \\
        \hline
        \no & \yes
      \end{tabular}
      \quad
      \begin{tabular}{c ? c}
        \textbf{Input} & \textbf{Output} \\
        \thickhline
        \yes & \no \\
        \hline
        \no & \no
      \end{tabular}
      \quad
      \begin{tabular}{c ? c}
        \textbf{Input} & \textbf{Output} \\
        \thickhline
        \yes & \yes \\
        \hline
        \no & \no
      \end{tabular}
      \quad
      \begin{tabular}{c ? c}
        \textbf{Input} & \textbf{Output} \\
        \thickhline
        \yes & \no \\
        \hline
        \no & \yes
      \end{tabular}
    \end{center}

    There are $4^2 = 16$ different lookup tables with 2 inputs, $4^3 = 64$ different lookup tables with 3 inputs, and so on.

    Some of these lookup tables have traditional operator names, like the lookup tables for $\negate$ and $\wedge$. Other possible lookup tables have no common operator names, but they're still valid lookup tables.


  \subsection{Calculating with closed propositions}

    To \vocab{calculate} the \vocab{value} of a \vocab{closed} \vocab{Boolean-proposition}, we can ``fold up'' its \vocab{syntax-tree} using the \vocabs{lookup-table} that define \vocab{Boolean-logic}. We calculate the value of a syntax tree \insist{bottom-up}, rewriting subtrees using lookup tables.

    For example, consider the syntax tree for the proposition $\yes \wedge \negate \no$:

    \begin{center}
      \begin{forest}
        [$\position{\wedge}\position$
          [\yes]
          [${\negate}\position$
            [\no]
          ]
        ]
      \end{forest}
    \end{center}

    The first step is to rewrite the $\negate \no$ subtree using the lookup table for the $\negate$ operator.

    \begin{center}
      \NOT
    \end{center}

    This tells us that $\negate \no \defequals \yes$, so we rewrite the $\negate \no$ subtree to a $\yes$ leaf.

    \begin{center}
      \begin{forest}
        [$\position{\wedge}\position$
          [\yes]
          [\yes]
        ]
      \end{forest}
    \end{center}

    The next step is to rewrite the $\yes \wedge \yes$ subtree using the lookup table for the $\wedge$ operator.

    \begin{center}
      \AND
    \end{center}

    This tells us that $\yes \wedge \yes \defequals \yes$, so we rewrite the $\yes \wedge \yes$ subtree to a $\yes$ leaf.

    \begin{center}
      \begin{forest}
        [\yes]
      \end{forest}
    \end{center}

    Now we're done: we've shown that the \vocab{value} of $\yes \wedge \negate \no$ is $\yes$.

    If there are multiple subtrees at the same level, it doesn't matter whether we calculate them left-to-right, or right-to-left, or in any other horizontal ordering. All that matters is that we calculate \insist{bottom-up}, so that we know the value of each subtree before we try to calculate the value of its parent node.

    \longdemo{Let's calculate the values of some more deeply-nested closed propositions, like $\negate \left(\yes \wedge \negate \left(\no \vee \yes\right)\right) \subset \left(\yes \subset \no\right)$.}


  \subsection{Calculating with open propositions}

    When we calculate the result of a \vocab{open} \vocab{Boolean-proposition}, we don't get a single \vocab{Boolean-value}: in general, we get a whole \vocab{lookup-table} as a result.

    For example, consider the open proposition $\negate \left(\metavar{a} \wedge \yes\right)$. Here is its syntax tree:

    \begin{center}
      \begin{forest}
        [${\negate}\position$
          [$\position{\wedge}\position$
            [\metavar{a}]
            [\yes]
          ]
        ]
      \end{forest}
    \end{center}

    To calculate the value of $\negate \left(\metavar{a} \wedge \yes\right)$, we start with a partial lookup table. We write a column for $\metavar{a}$ on the left of the thick vertical line, and a column for the proposition we're calculating on the right.

    \begin{center}
      \begin{tabular}{c ? c}
        $\metavar{a}$ & $\negate \left(\metavar{a} \wedge \yes\right)$ \\
        \thickhline
        \yes & \\
        \hline
        \no &
      \end{tabular}
    \end{center}

    To calculate the value of each \insist{row} in the table, we read the values on the left of the thick vertical line as a \vocab{substitution}, which gives us a \vocab{closed} expression in each cell.

    \begin{center}
      \begin{tabular}{c ? c}
        $\metavar{a}$ & $\negate \left(\metavar{a} \wedge \yes\right)$ \\
        \thickhline
        \yes & $\left(\negate \left(\metavar{a} \wedge \yes\right)\right)\left[\begin{array}{rcl} \metavar{a} & \mapsto & \yes \end{array}\right]$ \\
        \hline
        \no & $\left(\negate \left(\metavar{a} \wedge \yes\right)\right)\left[\begin{array}{rcl} \metavar{a} & \mapsto & \no \end{array}\right]$
      \end{tabular}
      \quad
      \begin{tabular}{c ? c}
        $\metavar{a}$ & $\negate \left(\metavar{a} \wedge \yes\right)$ \\
        \thickhline
        \yes & $\negate \left(\yes \wedge \yes\right)$ \\
        \hline
        \no & $\negate \left(\no \wedge \yes\right)$ \\
      \end{tabular}
    \end{center}

    Now we calculate the result of each closed expression:

    \begin{center}
      \begin{adjustbox}{valign=T}
        \begin{forest}
          [${\negate}\position$
            [$\position{\wedge}\position$
              [\yes]
              [\yes]
            ]
          ]
        \end{forest}
      \end{adjustbox}
        \quad
      \begin{adjustbox}{valign=T}
        \begin{forest}
          [${\negate}\position$
            [\yes]
          ]
        \end{forest}
      \end{adjustbox}
        \quad
      \begin{adjustbox}{valign=T}
        \begin{forest}
          [\no]
        \end{forest}
      \end{adjustbox}

      \begin{adjustbox}{valign=T}
        \begin{forest}
          [${\negate}\position$
            [$\position{\wedge}\position$
              [\no]
              [\yes]
            ]
          ]
        \end{forest}
      \end{adjustbox}
        \quad
      \begin{adjustbox}{valign=T}
        \begin{forest}
          [${\negate}\position$
            [\no]
          ]
        \end{forest}
      \end{adjustbox}
        \quad
      \begin{adjustbox}{valign=T}
        \begin{forest}
          [\yes]
        \end{forest}
      \end{adjustbox}
    \end{center}

    This gives us the completed lookup table for $\negate \left(\metavar{a} \wedge \yes\right)$.

    \begin{center}
      \begin{tabular}{c ? c}
        $\metavar{a}$ & $\negate \left(\metavar{a} \wedge \yes\right)$ \\
        \thickhline
        \yes & \no \\
        \hline
        \no & \yes
      \end{tabular}
    \end{center}

    If a proposition has more than one metavariable, we have one column for \insist{each} metavariable in the proposition. For example, here is the completed lookup table that we get from calculating $\negate \left(\metavar{a} \subset \metavar{b}\right)$.

    \begin{center}
      \begin{tabular}{c | c ? c}
        $\metavar{a}$ & $\metavar{b}$ & $\negate \left(\metavar{a} \subset \metavar{b}\right)$ \\
        \thickhline
        \yes & \yes & \no \\
        \hline
        \yes & \no & \yes \\
        \hline
        \no & \yes & \no \\
        \hline
        \no & \no & \no
      \end{tabular}
    \end{center}

    If the rightmost cell in \insist{every} row is $\yes$, the proposition is a \vocab{Boolean-tautology}. If the rightmost cell in \insist{every} row is $\no$, the proposition is a \vocab{Boolean-contradiction}. For example, $\metavar{a} \vee \negate \metavar{a}$ is a Boolean tautology and $\metavar{a} \wedge \negate \metavar{a}$ is a Boolean contradiction.

    \begin{center}
      \begin{tabular}{c ? c}
        $\metavar{a}$ & $\metavar{a} \vee \negate \metavar{a}$ \\
        \thickhline
        \yes & \yes \\
        \hline
        \no & \yes
      \end{tabular}
      \quad
      \begin{tabular}{c ? c}
        $\metavar{a}$ & $\metavar{a} \wedge \negate \metavar{a}$ \\
        \thickhline
        \yes & \no \\
        \hline
        \no & \no
      \end{tabular}
    \end{center}


\section{Meaning}

  How do we interpret the results of \vocab{Boolean-logic} in \vocab{proof-theory}?


  \subsection{Translating propositions}

    If we have a \vocab{Boolean-proposition}, we can replace $\yes$ with $\top$ and $\no$ with $\bot$, and replace each \vocab{metavariable} with an \vocab{arbitrary-constant}. This process gives us a valid proposition that we could try to prove in \acrshort{ipl} or \acrshort{cpl}.

    For example:
    \begin{itemize}
      \item $\yes \wedge \no$ becomes $\top \wedge \bot$
      \item $\negate \metavar{a} \vee \left(\negate \metavar{a} \subset \no\right)$ becomes $\negate \mono{P} \vee \left(\negate \mono{P} \subset \bot\right)$, or $\negate \mono{Q} \vee \left(\negate \mono{Q} \subset \bot\right)$, etc.
    \end{itemize}


  \subsection{Closed propositions}

    We have two very deep properties about \vocab{closed} Boolean propositions:
    \begin{itemize}
      \item A closed Boolean proposition has the value $\yes$ after \vocab{calculation} if and only if the corresponding proposition is a \vocab{tautology} in \acrshort{ipl}.
      \item A closed Boolean proposition has the value $\no$ after \vocab{calculation} if and only if the corresponding proposition is a \vocab{contradiction} in \acrshort{ipl}.
    \end{itemize}

    These are fairly sophisticated facts to prove, and they are potentially quite surprising! The Boolean system of \vocab{lookup-tables} has no obvious connection to the \vocabs{inference-rule} that define \acrshort{ipl}, but they give the \insist{same results} for all closed propositions.

    For example, if we calculate the \vocab{Boolean-value} of $\negate \left(\yes \wedge \no\right)$, we get the value $\yes$. This indicates without a doubt that the judgement $\emptyset \entails \negate \left(\top \wedge \bot\right)$ can be proven in \acrshort{ipl}.

    Note that the Boolean calculation does not directly tell us \insist{how} to write the corresponding \acrshort{ipl} proof! It just guarantees that a proof is \insist{possible}.


  \subsection{Open propositions}

    More generally, we have similar properties about \vocab{open} Boolean propositions:
    \begin{itemize}
      \item An open Boolean proposition is a \vocab{Boolean-tautology} if and only if the corresponding proposition is a \vocab{tautology} in \acrshort{cpl}.
      \item An open Boolean proposition is a \vocab{Boolean-contradiction} if and only if the corresponding proposition is a \vocab{contradiction} in \acrshort{cpl}.
    \end{itemize}

    Note that the property for \vocab{closed} propositions mentioned \acrshort{ipl}, but this one mentions \acrshort{cpl}!

    If some proposition $P$ is a Boolean tautology, then $\emptyset \entails P$ can be proven in \acrshort{cpl}, which means $\emptyset \entails \negate \negate P$ can be proven in \acrshort{ipl}. We \insist{might} also be able to prove $\emptyset \entails P$ in \acrshort{ipl}, but the Boolean tautology provides \insist{no guarantee} of this.

    For example, the open Boolean proposition $\negate \negate \metavar{a} \subset \metavar{a}$ is a Boolean tautology, which we show by calculating its lookup table.

    \begin{center}
      \begin{tabular}{c ? c}
        $\metavar{a}$ & $\negate \negate \metavar{a} \subset \metavar{a}$ \\
        \thickhline
        \yes & \yes \\
        \hline
        \no & \yes
      \end{tabular}
    \end{center}

    This indicates that $\emptyset \entails \negate \negate \metavar{a} \subset \metavar{a}$ can be proven in \acrshort{cpl}, which means $\emptyset \entails \negate \negate \left(\negate \negate \metavar{a} \subset \metavar{a}\right)$ can be proven in \acrshort{ipl}. As we've seen before, $\emptyset \entails \negate \negate \metavar{a} \subset \metavar{a}$ \insist{cannot} be proven in \acrshort{ipl} (it is \vocab{independent-of} \acrshort{ipl}).

    Like with closed propositions, the Boolean tautology does not directly tell us \insist{how} to write the corresponding proof, it just guarantees that a proof is \insist{possible}. This is still pretty a remarkable connection, though!



\input{include/footer.tex}
\end{document}
