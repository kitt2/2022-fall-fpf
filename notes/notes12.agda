{-
This set of notes covers our lecture discussions on the topic of lambda binding.
-}
module notes12 where

{-
First, we'll restate our definitions of the basic STLC types. We could import
these from the notes11 module instead of re-defining them, but it's nice to have
the content all in one file for the sake of studying and review.
-}

data Unit : Set where
  unit : Unit

data _×_ (A B : Set) : Set where
  _,_ : A → B → A × B

data _⊎_ (A B : Set) : Set where
  inj₁ : A → A ⊎ B
  inj₂ : B → A ⊎ B

variable
  P Q R S : Set


{-
Remember that Agda's built-in _→_ operator corresponds to our implication
operator in IPL (_⊂_). We've seen before that *naming* an argument in a proof
corresponds to ⊂-Intro, and the invisible "space operator" corresponds to
⊂-Elim.

More specifically, naming arguments on the left side of the = symbol corresponds
to using ⊂-Intro rules as the *first* rules in a proof (reading bottom-up). For
example, consider this proof:
-}
example₁ : P → (Q → P ⊎ Q)
example₁ x y = inj₁ x

{-
The proof of example₁ on paper (reading bottom-up) uses two ⊂-Intro rules
followed by a ∨-Introᴸ rule. Naming x corresponds to the first ⊂-Intro rule and
naming y corresponds to the second. This is valid in example₁ because there are
no rules *below* the two ⊂-Intro rules.

In contrast, consider the proposition (P ⊂ ⊤) ∨ (⊤ ⊂ P). On paper (still reading
bottom-up), we can prove this proposition with ∨-Introᴸ followed by ⊂-Intro and
then ⊤-Intro.

There *is* a rule below the ⊂-Intro rule in this proof, so we *cannot* implement
the proof in Agda by naming the argument on the left side of the = symbol! Try
uncommenting the proof below to see the error:
-}
-- example₂ : (P → Unit) ⊎ (Unit → P)
-- example₂ x = inj₁ {!!} -- error!


{-
In fact, the notation where we name arguments on the left side of the = symbol
is just a convenient *shorthand* for the most common use of ⊂-Intro. It does not
cover all possible uses of ⊂-Intro on paper.

The shorthand form expands to a more primitive notation which *directly*
corresponds to the ⊂-Intro rule on paper, and can be used *everywhere* that the
⊂-Intro rule is valid. This primitive notation is called "lambda binding".

In Agda, we write a lambda binding with this notation:

  λ x → e

The → symbol in a lambda binding is just *punctuation*: it doesn't really mean
anything by itself. It's different than the _→_ type operator, which (usually)
only shows up to the right of the _:_ operator in a type signature.

The λ symbol indicates that we are introducing an implication. Wherever we see a
λ in an Agda proof, there will be a corresponding ⊂-Intro rule in the on-paper
proof.

In between the λ symbol and the → symbol, we can *choose* any variable name that
we want. This is the name that will correspond to the Assumed rule for the
assumption that we're introducing.

On the right side of the → symbol, we write the proof that goes above the
⊂-Intro rule.

For example, here's the definition of example₁ using the more primitive lambda
binding notation, along with a full definition of example₂.
-}
example₁-λ : P → (Q → P ⊎ Q)
example₁-λ = λ a → (λ b → inj₁ a)

example₂-λ : (P → Unit) ⊎ (Unit → P)
example₂-λ = inj₁ (λ v → unit)


{-
If you're familiar with functional programming, lambda bindings will probably
feel familiar. In particular, Agda's lambda bindings work very similar to
Haskell's.

You don't need to know functional programming in order to use lambda bindings in
proofs, though! Just remember that the lambda notation corresponds to ⊂-Intro,
and the name that you choose in the lambda binding corresponds to the Assumed
rule for that assumption.

Lambda bindings are pretty common in Agda proofs, so Agda has a lot of shorthand
to make them more convenient. Unfortunately, this can make some Agda code very
hard for beginners to read.

In these notes, the only lambda-related shorthand we'll use is introducing names
to the left side of the = sign, which corresponds to using a lambda binding at
the *outermost* level of the proof. Specifically:

  proof var = expr          corresponds to    proof = λ var → expr
  proof var₁ var₂ = expr    corresponds to    proof = λ var₁ → (λ var₂ → expr)
                            etc.
-}

{-
What about pattern-matching? We can pattern-match on the left side of the =
symbol in a definition, for example to "case over" a ⊎ proof to find out whether
it was constructed with inj₁ or inj₂.
-}
example₄ : (P ⊎ Q) → (Q ⊎ P)
example₄ (inj₁ a) = inj₂ a
example₄ (inj₂ b) = inj₁ b

{-
We can pattern match in a similar way with lambda binding notation. In general,
the notation is "λ where" followed by the cases, which are separated with
newlines.
-}
example₄-λ : (P ⊎ Q) → (Q ⊎ P)
example₄-λ =
  λ where
    (inj₁ a) → inj₂ a
    (inj₂ b) → inj₁ b

{-
(The cases in the "where" can also be separated by semicolons instead of
newlines, if you want to write a pattern-matching lambda on a single line.)
-}


{-
In IPL on paper, we've seen the property of "currying", which is a particular
algebraic property relating the _⊂_ and _∧_ operators:

  ∅ ⊢ (P ⊂ (Q ⊂ R)) ⊂ ((P ∧ Q) ⊂ R)
  ∅ ⊢ ((P ∧ Q) ⊂ R) ⊂ (P ⊂ (Q ⊂ R))

In other words, P ⊂ (Q ⊂ R) is equivalent to (P ∧ Q) ⊂ R: if either one is
provable, then both are provable.

Intuitively, this makes sense by our logical readings of the propositions:

  P ⊂ (Q ⊂ R) is "if P is true, then if Q is true, then R is true"
  (P ∧ Q) ⊂ R is "if P and Q are true, then R is true"

In Agda, this means that P → (Q → R) and (P × Q) → R are equivalent, and we can
"convert" back and forth between them easily. We traditionally name these
"conversion" proofs after the logician Haskell Curry (not after the spice,
disappointingly).
-}
curry : ((P × Q) → R) → (P → (Q → R))
-- f : (P × Q) → R
-- x : P
-- y : Q
-- (x , y) : P × Q
-- f (x , y) : R
-- (λ y → f (x , y)) : Q → R
-- (λ x → (λ y → f (x , y))) : P → (Q → R)
curry = λ f → (λ x → (λ y → f (x , y)))

uncurry : (P → (Q → R)) → ((P × Q) → R)
-- f : P → (Q → R)
-- x : P
-- y : Q
-- f x : Q → R
-- (f x) y : R
-- (λ where (x , y) → (f x) y) : (P × Q) → R
uncurry =
  λ f → λ where
    (x , y) → (f x) y

{-
In STLC, we read _→_ as a one-argument *function* type:

  P → Q is "if you give me a P, then I can give you a Q"

Under this reading:

  P → (Q → R) is "if you give me a P,
                  then if you give me a Q,
                  then I can give you an R"

  (P × Q) → R is "if you give me a P and a Q,
                  then I can give you an R"

These are two different ways of "simulating" two-argument functions with
one-argument functions. We can either pass the two arguments one at a time, or
we can pass them both at the same time by wrapping them up in a single data
structure.

To describe these two different ways of handling arguments, we use terminology
based on the property of currying:

  P → (Q → R) is a "curried function type"
  (P × Q) → R is an "uncurried function type"

What does it look like when we *call* a curried or uncurried function?

Calling a curried function involves a nested use of the "space operator":

  f : P → (Q → R)
  x : P
  y : Q
  (f x) : Q → R
  (f x) y : R

Calling an uncurried function involves the _,_ constructor:

  f : (P × Q) → R
  x : P
  y : Q
  (x , y) : P × Q
  f (x , y) : R

The uncurried form looks more like a traditional function call, but in practice
the curried form is usually more convenient in Agda, because it doesn't involve
an additional constructor.

For this reason, Agda has special shorthand for calling curried functions: the
"space operator" is *left-associative*, which means its syntax trees always
"lean to the left":

  "f x y"    is    space     the same as "(f x) y"
                   /   \     *different* from "f (x y)"
                 space  y
                  / \
                 f   x

Similarly, the _→_ operator is *right-associative*, which means its syntax trees
always "lean to the right":

  "P → Q → R"    is    _→_    the same as "P → (Q → R)"
                      /   \   *different* from "(P → Q) → R"
                     P    _→_
                          / \
                         Q   R

These two shorthands work very nicely together:

  f : P → Q → R
  x : P
  y : Q
  f x y : R

We will use curried shorthand in these notes, so make sure to come back and
review these principles if you ever get confused about how to read function
types and function calls.
-}
