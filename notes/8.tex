\input{include/header.tex}

% auto-generated glossary terms
\input{notes/8.glt}

% rules of inference
\input{include/theories/iplc.tex}


\indextitle{Absurdity and negation}
\subtitle{Disproving false things}

\makeglossaries


\begin{document}

\maketitle
\tableofcontents

\newpage



\section{Introduction}

  In logical settings, we traditionally use the word \vocab{negation} to refer to the logical ``NOT'' operator, which ``negates'' the truth value of a proposition. When we talk about a \vocab{negative-proposition}, we're referring to a proposition under a ``NOT'', which is a proposition that we're trying to prove \vocab{false} instead of proving \vocab{true}.

  There's an old saying in English that goes ``you can't prove a negative'', often used in a similar way as the phrase ``absence of evidence is not evidence of absence''. The idea is roughly that you can't prove the non-existence of some object or fact just by searching very hard for it and failing to find it, because it might still be somewhere you haven't looked yet.

  This idea can have some meaningful degree of truth when applied to empirical measurements, but it's demonstrably false in most systems of \vocab{formal-logic}. We have a precise definition of what it means to prove a negative proposition, or to \vocab{disprove} a proposition.

  For now, since we still don't have any interesting mathematical objects to reason about, the kinds of things we can disprove will be relatively limited. Relatively soon, we'll formally introduce \vocabs{natural-number} and have many things to disprove.



\section{Truth}

  Remember $\top$, the ``trivially true'' proposition which is provable in any context:

  \begin{center}
    \TopIntro
  \end{center}

  Other propositions like $\mono{P}$ or $\mono{P} \wedge \mono{Q}$ are only provable in \insist{some} contexts. This means that $\top$ can serve as a kind of global ``standard of truth'' throughout our proof system.

  One way to read $P \subset Q$ is ``$Q$ is at least as provable as $P$'': in other words, if $P$ is provable then $Q$ must be provable. In an \vocab{entailment-judgement}, $C \entails P \subset Q$ means that if $P$ is provable in $C$ then $Q$ must be provable in $C$.

  We can show that $\top \subset \mono{P}$ is provable in any context exactly when \mono{P} is provable in that context, meaning $\top \subset \mono{P}$ and $\mono{P}$ are \vocab{equivalent} propositions:

  \begin{center}
            \AxiomC{}
          \LeftLabel{\inAt{1}}
          \UnaryInfC{$\mono{P} \in \top \cons \mono{P} \cons \metavar{\Gamma}$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\top \cons \mono{P} \cons \metavar{\Gamma} \entails \mono{P}$}
      \LeftLabel{\implIntro}
      \UnaryInfC{$\mono{P} \cons \metavar{\Gamma} \entails \top \subset \mono{P}$}
    \LeftLabel{\implIntro}
    \UnaryInfC{$\metavar{\Gamma} \entails \mono{P} \subset \left(\top \subset \mono{P}\right)$}
    \DisplayProof

    \smallskip

            \AxiomC{}
          \LeftLabel{\inAt{0}}
          \UnaryInfC{$\top \subset \mono{P} \in \top \subset \mono{P} \cons \metavar{\Gamma} \entails \top \subset \mono{P}$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\top \subset \mono{P} \cons \metavar{\Gamma} \entails \top \subset \mono{P}$}
          \AxiomC{}
        \RightLabel{\topIntro}
        \UnaryInfC{$\top \subset \mono{P} \cons \metavar{\Gamma} \entails \top$}
      \LeftLabel{\implElim}
      \BinaryInfC{$\top \subset \mono{P} \cons \metavar{\Gamma} \entails \mono{P}$}
    \LeftLabel{\implIntro}
    \UnaryInfC{$\metavar{\Gamma} \entails \left(\top \subset \mono{P}\right) \subset \mono{P}$}
    \DisplayProof
  \end{center}

  This aligns with the ``at least as provable'' reading: $\top$ is always provable in any context, so $\top \subset \mono{P}$ is saying ``if `the thing that is always provable' is provable, then $P$ must be provable'\thinspace''.

  In this sense, we could actually modify our definition of \vocab{truth} to say that a proposition $P$ is \vocab{true} if and only if $\top \subset P$ is provable. This is equivalent to saying that $P$ is true if and only if $P$ is provable.

  The fact that we can define the truth of $P$ in terms of $\top \subset P$ is our intuition for thinking of $\top$ as the ``standard of truth'' within \acrshort{ipl}.



\section{Absurdity}

  To define when a proposition is \vocab{false}, we can define a kind of ``standard of falsehood'' that is ``opposite'' to $\top$, our ``standard of truth''.

  In the tradition of \vocab{constructive-logic}, this ``standard of falsehood'' is often called \vocab{absurdity}. We also call it ``bottom'', which is the name of the symbol we use to write it, \vocab{sym-bot}.

  We'll see the \vocab{elimination-rule} for $\bot$ later in these notes. There is \insist{no \vocab{introduction-rule}} for $\bot$, just like there is no \vocab{elimination-rule} for $\top$.


  \subsection{Consistency}

    In an \vocab{empty-context}, $\bot$ has a very straightforward meaning: because there is no introduction rule, we can never produce a proof of $\emptyset \entails \bot$, meaning $\bot$ is a kind of ``anti-\vocab{tautology}''. This property of \acrshort{ipl} is known as \vocab{consistency}, or more specifically as \vocab{internal-consistency}.

    We haven't introduced the techniques needed to prove \vocab{internal-consistency} for \acrshort{ipl}, and it's actually a relatively sophisticated proof. The intuition is not too complex, though: $\bot$ has no \vocab{introduction-rule}, and there is no way to ``extract'' it from an \vocab{empty-context} using any \vocab{elimination-rule}.

    In a non-empty context, $\bot$ is a strange proposition with deep subtleties. There are some contexts in which we can prove $\bot$ and some in which we can't. Most simply, we can prove $\bot \cons \emptyset \entails \bot$.

    \begin{prooftree}
          \AxiomC{}
        \LeftLabel{\inAt{0}}
        \UnaryInfC{$\bot \in \bot \cons \emptyset$}
      \LeftLabel{\assumed}
      \UnaryInfC{$\bot \cons \emptyset \entails \bot$}
    \end{prooftree}

    A little more subtly, we can prove $\bot$ in \insist{some} contexts with implications that have $\bot$ on the right side of the $\subset$ symbol.

    \begin{prooftree}
            \AxiomC{}
          \LeftLabel{\inAt{0}}
          \UnaryInfC{$\left(\mono{P} \subset \mono{P}\right) \subset \bot \in \left(\mono{P} \subset \mono{P}\right) \subset \bot \cons \emptyset$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\left(\mono{P} \subset \mono{P}\right) \subset \bot \cons \emptyset \entails \left(\mono{P} \subset \mono{P}\right) \subset \bot$}
              \AxiomC{}
            \RightLabel{\inAt{0}}
            \UnaryInfC{$\mono{P} \in \mono{P} \cons \left(\mono{P} \subset \mono{P}\right) \subset \bot \cons \emptyset$}
          \RightLabel{\assumed}
          \UnaryInfC{$\mono{P} \cons \left(\mono{P} \subset \mono{P}\right) \subset \bot \cons \emptyset \entails \mono{P}$}
        \RightLabel{\implIntro}
        \UnaryInfC{$\left(\mono{P} \subset \mono{P}\right) \subset \bot \cons \emptyset \entails \left(\mono{P} \subset \mono{P}\right)$}
      \LeftLabel{\implElim}
      \BinaryInfC{$\left(\mono{P} \subset \mono{P}\right) \subset \bot \cons \emptyset \entails \bot$}
    \end{prooftree}

    But this is not always true: there are also \insist{some} contexts which mention $\bot$ but do not entail $\bot$. For example, we can't prove $\mono{P} \vee \bot \cons \emptyset \entails \bot$, because we can't complete the middle branch of the necessary \orElim\ rule.

    \begin{prooftree}
            \AxiomC{}
          \LeftLabel{\inAt{0}}
          \UnaryInfC{$\mono{P} \vee \bot \in \mono{P} \vee \bot \cons \emptyset$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\mono{P} \vee \bot \cons \emptyset \entails \mono{P} \vee \bot$}
          \AxiomC{\color{red} ???}
          \LeftLabel{\color{red} ???}
        \UnaryInfC{\color{black} $\mono{P} \cons \mono{P} \vee \bot \cons \emptyset \entails \bot$}
            \AxiomC{}
          \LeftLabel{\inAt{0}}
          \UnaryInfC{$\bot \in \bot \cons \mono{P} \vee \bot \cons \emptyset$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\bot \cons \mono{P} \vee \bot \cons \emptyset \entails \bot$}
      \LeftLabel{\orElim}
      \TrinaryInfC{$\mono{P} \vee \bot \cons \emptyset \entails \bot$}
    \end{prooftree}

    If we cannot prove $C \entails \bot$, we say that $C$ is a \vocab{consistent} context. In a consistent context, we can generally imagine that we're working in the \vocab{positive-fragment} of \acrshort{ipl}, and our proofs generally have the same meaning as before.

    If we can prove $C \entails \bot$ for some particular context $C$, we say that $C$ is an \vocab{inconsistent} context. Intuitively, a context is \vocab{inconsistent} if we can ``combine'' its assumptions to produce a proof of $\bot$. In an inconsistent context, our proofs take on a somewhat different meaning.

    Remember that when we have a \vocab{tautology} proof for each assumption in a context $C$, and a proof of an entailment $C \entails P$, we can use the \satisfy\ rule to ``plug in'' the subproof for each assumption and produce a proof that $P$ is a tautology ($\emptyset \entails P$).

    If a context $C$ is inconsistent, then it is \insist{impossible} to construct tautology proofs for every assumption in $C$. This is critical: a proof in an inconsistent context cannot be ``converted'' to a tautology with \satisfy. This is not a new restriction on the \satisfy\ rule, it's a \insist{consequence} of \vocab{internal-consistency}.

    In a sense, the ``point'' of our proof system is to produce proofs of tautologies. This means that a proof whose conclusion has an inconsistent context is a form of ``dead code'': we can never ``finish'' it by proving all of its assumptions.

    Remember that different parts of a proof may have different contexts, though. A proof of a tautology \insist{may} involve an inconsistent context somewhere, just not in the conclusion. This situation will sometimes arise when proving \vocabs{negative-proposition}.


  \subsection{Elimination rule}

    The \botElim\ rule is what makes $\bot$ strange and subtle. It may seem unintuitive at first, but it has a justification in our definition of an \vocab{inconsistent} \vocab{context}.

    \begin{center}
      \BotElim
    \end{center}

    Reading top-down, the \botElim\ rule says ``if we can prove $\bot$ in some context, we can prove anything in that context''. Reading bottom-up, \botElim\ says ``to prove something in some context, you can prove $\bot$ in that context''.

    The trick is that we can \insist{only} prove $\bot$ in an \vocab{inconsistent} context, by definition. This means that when constructing a proof tree bottom-up, \botElim\ can \insist{never} be productively used in a \vocab{consistent} context, by definition. A consequence is that we can never use \botElim\ to directly conclude a \vocab{tautology}, since $\emptyset$ is a \vocab{consistent} context.

    In a certain intuitive sense, when we use the \botElim\ rule we're saying ``this is dead code, so I don't have to justify anything that I do''. We know that any branch of the proof tree using \botElim\ will not directly conclude a \vocab{tautology}, so we can prove anything we want in those branches and it won't ``infect'' any tautology proofs with nonsense.



\section{Negation}

  \subsection{Intuition}

    With our definition of \vocab{absurdity} in hand, we're ready to define \vocab{negation}.

    As we saw before, we can define the \vocab{truth} of a proposition relative to $\top$ by saying that $P$ is true if $\top \subset P$ is provable. The intuition is that we're saying $P$ is ``at least as provable'' as something that we know to be provable.

    Since we know $\bot$ is unprovable in a consistent context, we can say that a proposition $P$ is \vocab{disprovable} in some context $C$ if $C \entails P \subset \bot$ is provable. To justify this, we can read $P \subset \bot$ as saying ``$\bot$ is at least as provable as $P$'', which means $P$ must be unprovable in a consistent context.

  \subsection{Definition}

    As shorthand, we define the ${\negate}\position$ operator for \vocab{negation}: the expression $\negate P$ (pronounced ``\vocab{not} $P$'') is shorthand for $P \subset \bot$.

    For example:
    \begin{itemize}
      \item $\negate \left(P \wedge Q\right)$ is shorthand for $\left(P \wedge Q\right) \subset \bot$
      \item $\negate P \wedge \negate Q$ is shorthand for $\left(P \subset \bot\right) \wedge \left(Q \subset \bot\right)$
      \item $\negate \negate P$ is shorthand for $\left(P \subset \bot\right) \subset \bot$
    \end{itemize}

    We can define this shorthand with two \vocabs{derived-rule} for negation:

    \begin{center}
      \NotIntro \quad \NotElim
    \end{center}

    Unlike some of our other derived rules, it's pretty easy to justify these formally by just ``expanding out'' the shorthand.

    \begin{center}
      \AxiomC{$\metavar{a} \cons \metavar{\Gamma} \vdash \bot$}
      \LeftLabel{\notIntro}
      \UnaryInfC{$\metavar{\Gamma} \vdash \metavar{a} \subset \bot$}
      \DisplayProof
      \quad
      \AxiomC{$\metavar{\Gamma} \vdash \metavar{a} \subset \bot$}
      \AxiomC{$\metavar{\Gamma} \vdash \metavar{a}$}
      \LeftLabel{\notElim}
      \BinaryInfC{$\metavar{\Gamma} \vdash \bot$}
      \DisplayProof
    \end{center}

    With this presentation, we can see that \notIntro\ is just a special case of \implIntro\ and \notElim\ is just a special case of \implElim.



\section{Falsehood}


  We use \vocab{negation} to define when a proposition is \vocab{false}. When a proposition is \vocab{false}, we say that it has the property of \vocab{falsehood} (or \vocab{falsity}). We also sometimes use the word \vocab{falsehood} to refer to the false proposition itself, as in ``$P$ is a falsehood''. (This is like how the word \vocab{truth} works. \Vocab{false} is the opposite of \vocab{true}, \vocab{falsehood} is the opposite of \vocab{truth}.)

  A proposition $P$ is \vocab{false} exactly when $\negate P$ is \vocab{true}. By our definition of \vocab{truth}, this means that $P$ is \vocab{false} exactly when we can prove $\negate P$.

  More precisely, if we can prove the judgement $C \entails \negate P$ for some \vocab{context} $C$ and \vocab{proposition} $P$, we might say ``$P$ is false if everything in $C$ is true'', or ``if I have proofs of everything in $C$ then I can \vocab{disprove} $P$''.


  \subsection{Contradiction}

    If we can prove $\emptyset \entails \negate P$ for some proposition $P$, we say $P$ is a \vocab{contradiction}, the opposite of a \vocab{tautology}. In other words, if $\negate P$ is a \vocab{tautology}, then $P$ is a \vocab{contradiction}.

    $\bot$ is a contradiction: this is another way to phrase the property of \vocab{internal-consistency}.

    More generally, $\mono{P} \wedge \negate \mono{P}$ is an important contradiction in \acrshort{ipl}:

    \begin{prooftree}
                \AxiomC{}
              \LeftLabel{\inAt{0}}
              \UnaryInfC{$\mono{P} \wedge \negate \mono{P} \in \mono{P} \wedge \negate \mono{P} \cons \emptyset$}
            \LeftLabel{\assumed}
            \UnaryInfC{$\mono{P} \wedge \negate \mono{P} \cons \emptyset \entails \mono{P} \wedge \negate \mono{P}$}
          \LeftLabel{\andElimR}
          \UnaryInfC{$\mono{P} \wedge \negate \mono{P} \cons \emptyset \entails \negate \mono{P}$}
                \AxiomC{}
              \RightLabel{\inAt{0}}
              \UnaryInfC{$\mono{P} \wedge \negate \mono{P} \in \mono{P} \wedge \negate \mono{P} \cons \emptyset$}
            \RightLabel{\assumed}
            \UnaryInfC{$\mono{P} \wedge \negate \mono{P} \cons \emptyset \entails \mono{P} \wedge \negate \mono{P}$}
          \RightLabel{\andElimL}
          \UnaryInfC{$\mono{P} \wedge \negate \mono{P} \cons \emptyset \entails \mono{P}$}
        \LeftLabel{\notElim}
        \BinaryInfC{$\mono{P} \wedge \negate \mono{P} \cons \emptyset \entails \bot$}
      \LeftLabel{\notIntro}
      \UnaryInfC{$\emptyset \entails \negate (\mono{P} \wedge \negate \mono{P})$}
    \end{prooftree}

    This aligns with the usual logical intuition that no proposition is both true and false at the same time.

    Note that this intuition is only valid in an \vocab{empty-context}! There is nothing stopping us from \insist{assuming} that \mono{P} is \vocab{true} and \vocab{false} at the same time, in which case we \insist{can} prove $\mono{P} \wedge \negate \mono{P}$.

    \begin{prooftree}
            \AxiomC{}
          \LeftLabel{\inAt{0}}
          \UnaryInfC{$\mono{P} \in \mono{P} \cons \negate \mono{P} \cons \emptyset$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\mono{P} \cons \negate \mono{P} \cons \emptyset \entails \mono{P}$}
            \AxiomC{}
          \RightLabel{\inAt{1}}
          \UnaryInfC{$\negate \mono{P} \in \mono{P} \cons \negate \mono{P} \cons \emptyset$}
        \RightLabel{\assumed}
        \UnaryInfC{$\mono{P} \cons \negate \mono{P} \cons \emptyset \entails \negate \mono{P}$}
      \LeftLabel{\andIntro}
      \BinaryInfC{$\mono{P} \cons \negate \mono{P} \cons \emptyset \entails \mono{P} \wedge \negate \mono{P}$}
    \end{prooftree}

    This might seem a little surprising, but it's only provable because \mbox{$\mono{P} \cons \negate \mono{P} \cons \emptyset$} is an \vocab{inconsistent} context.

    \begin{prooftree}
            \AxiomC{}
          \LeftLabel{\inAt{1}}
          \UnaryInfC{$\negate \mono{P} \in \mono{P} \cons \negate \mono{P} \cons \emptyset$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\mono{P} \cons \negate \mono{P} \cons \emptyset \entails \negate \mono{P}$}
            \AxiomC{}
          \RightLabel{\inAt{0}}
          \UnaryInfC{$\mono{P} \in \mono{P} \cons \negate \mono{P} \cons \emptyset$}
        \RightLabel{\assumed}
        \UnaryInfC{$\mono{P} \cons \negate \mono{P} \cons \emptyset \entails \mono{P}$}
      \LeftLabel{\notElim}
      \BinaryInfC{$\mono{P} \cons \negate \mono{P} \cons \emptyset \entails \bot$}
    \end{prooftree}

    Remember, because of the \botElim\ rule, we can prove \insist{anything} in an inconsistent context.

    \begin{prooftree}
              \AxiomC{}
            \LeftLabel{\inAt{1}}
            \UnaryInfC{$\negate \mono{P} \in \mono{P} \cons \negate \mono{P} \cons \emptyset$}
          \LeftLabel{\assumed}
          \UnaryInfC{$\mono{P} \cons \negate \mono{P} \cons \emptyset \entails \negate \mono{P}$}
              \AxiomC{}
            \RightLabel{\inAt{0}}
            \UnaryInfC{$\mono{P} \in \mono{P} \cons \negate \mono{P} \cons \emptyset$}
          \RightLabel{\assumed}
          \UnaryInfC{$\mono{P} \cons \negate \mono{P} \cons \emptyset \entails \mono{P}$}
        \LeftLabel{\notElim}
        \BinaryInfC{$\mono{P} \cons \negate \mono{P} \cons \emptyset \entails \bot$}
      \LeftLabel{\botElim}
      \UnaryInfC{$\mono{P} \cons \negate \mono{P} \cons \emptyset \entails \mono{Q}$}
    \end{prooftree}

    This is why \vocabs{tautology} and \vocabs{contradiction} are particularly important to us: by requiring a conclusion in an empty context, we don't have to worry about inconsistent contexts ``infecting'' our proofs by surprise.



\section{Double negation}


  \subsection{Introduction}

    We can prove the \vocab{tautology} that if \mono{P} is \vocab{true}, it is \vocab{not} \vocab{false}. This is a reassuring fact about our definitions of \vocab{not}, \vocab{true}, and \vocab{false}.

    \begin{prooftree}
                \AxiomC{}
              \LeftLabel{\inAt{0}}
              \UnaryInfC{$\negate \mono{P} \in \negate \mono{P} \cons \mono{P} \cons \emptyset$}
            \LeftLabel{\assumed}
            \UnaryInfC{$\negate \mono{P} \cons \mono{P} \cons \emptyset \entails \negate \mono{P}$}
                \AxiomC{}
              \RightLabel{\inAt{1}}
              \UnaryInfC{$\mono{P} \in \negate \mono{P} \cons \mono{P} \cons \emptyset$}
            \RightLabel{\assumed}
            \UnaryInfC{$\negate \mono{P} \cons \mono{P} \cons \emptyset \entails \mono{P}$}
          \LeftLabel{\notElim}
          \BinaryInfC{$\negate \mono{P} \cons \mono{P} \cons \emptyset \entails \bot$}
        \LeftLabel{\notIntro}
        \UnaryInfC{$\mono{P} \cons \emptyset \entails \negate \negate \mono{P}$}
      \LeftLabel{\implIntro}
      \UnaryInfC{$\emptyset \entails \mono{P} \subset \negate \negate \mono{P}$}
    \end{prooftree}

    We might refer to this as a principle of ``\vocab{double-negation} introduction'', because we can use it along with \implElim\ to ``convert'' a proof of $P$ into a proof of $\negate \negate P$.


  \subsection{Elimination}

    It will probably be surprising that we \insist{cannot} prove $\emptyset \entails \negate \negate \mono{P} \subset \mono{P}$ in \acrshort{ipl}. We might refer to this proposition as a general principle of ``\vocab{double-negation} elimination'': it says that for any arbitrary proposition \mono{P}, if \mono{P} is not \vocab{false}, then it must be \vocab{true}.

    We can start the proof:

    \begin{prooftree}
          \AxiomC{???}
        \LeftLabel{???}
        \UnaryInfC{$\negate \negate \mono{P} \cons \emptyset \entails \mono{P}$}
      \LeftLabel{\implIntro}
      \UnaryInfC{$\emptyset \entails \negate \negate \mono{P} \subset \mono{P}$}
    \end{prooftree}

    We can try to make progress with \botElim, \notElim, and \notIntro:

    \begin{prooftree}
                \AxiomC{}
              \LeftLabel{\inAt{0}}
              \UnaryInfC{$\negate \negate \mono{P} \in \negate \negate \mono{P} \cons \emptyset$}
            \LeftLabel{\assumed}
            \UnaryInfC{$\negate \negate \mono{P} \cons \emptyset \entails \negate \negate \mono{P}$}
                \AxiomC{???}
              \RightLabel{???}
              \UnaryInfC{$\mono{P} \cons \negate \negate \mono{P} \cons \emptyset \entails \bot$}
            \RightLabel{\notIntro}
            \UnaryInfC{$\negate \negate \mono{P} \cons \emptyset \entails \negate \mono{P}$}
          \LeftLabel{\notElim}
          \BinaryInfC{$\negate \negate \mono{P} \cons \emptyset \entails \bot$}
        \LeftLabel{\botElim}
        \UnaryInfC{$\negate \negate \mono{P} \cons \emptyset \entails \mono{P}$}
      \LeftLabel{\implIntro}
      \UnaryInfC{$\emptyset \entails \negate \negate \mono{P} \subset \mono{P}$}
    \end{prooftree}

    But we get totally stuck at this point: we need to prove $\bot$ in a consistent context. \mono{P} does not contradict with $\negate \negate \mono{P}$.

    So $\negate \negate \mono{P} \subset \mono{P}$ is not a \vocab{tautology}. Is it a \vocab{contradiction}? Nope:

    \begin{prooftree}
          \AxiomC{???}
        \LeftLabel{???}
        \UnaryInfC{$\negate \negate \mono{P} \subset \mono{P} \cons \emptyset \entails \bot$}
      \LeftLabel{\notIntro}
      \UnaryInfC{$\emptyset \entails \negate \left(\negate \negate \mono{P} \subset \mono{P}\right)$}
    \end{prooftree}

    We can try to make progress with \implElim, but we'll find ourselves stuck trying to prove \mono{P} with no helpful assumptions. (Try it yourself to see exactly where the proof gets stuck.)

    By this reasoning, we show that in \acrshort{ipl}, $\negate \negate \mono{P} \subset \mono{P}$ is \insist{neither} a \vocab{tautology} \insist{nor} a \vocab{contradiction}. We can't prove this property within \acrshort{ipl} itself, but we can prove it \insist{about} \acrshort{ipl} using a stronger proof system.

    What \insist{is} it, then?


  \subsection{Classical truth}

    In \acrshort{ipl}, a proposition may be neither \vocab{true} nor \vocab{false}. This seems strange from the perspective of Boolean logic, but \acrshort{ipl} is not quite Boolean logic!

    To be precise, if we can prove $\emptyset \entails P$, we might say that $P$ is an \vocab{intuitionistic-tautology}. If we can prove $\emptyset \entails \negate \negate P$, we might say that $P$ is a \vocab{classical-tautology}. We can define the terms \vocab{intuitionistically-true} and \vocab{classically-true} similarly.

    Because of the ``double-negation introduction'' principle, every \vocab{intuitionistic-tautology} is a \vocab{classical-tautology}. More generally, any proposition that is \vocab{intuitionistically-true} in some context is also \vocab{classically-true} in that context.

    Because \acrshort{ipl} does \insist{not} have a ``double-negation elimination'' principle, \insist{not} every \vocab{classical-tautology} is an \vocab{intuitionistic-tautology}. $\negate \negate \mono{P} \subset \mono{P}$ is a classical tautology, but not an intuitionistic tautology.

    \longdemo{Let's prove that $\negate \negate \mono{P} \subset \mono{P}$ is a classical tautology by proving $\emptyset \entails \negate \negate \left(\negate \negate \mono{P} \subset \mono{P}\right)$.}

    In mathematical terminology, we might sometimes say that the ``double-negation elimination'' principle is \vocab{independent-of} \acrshort{ipl}. This means that it is \vocab{consistent} in \acrshort{ipl} to \vocab{assume} either $\negate \negate \mono{P} \subset \mono{P}$ or $\negate \left(\negate \negate \mono{P} \subset \mono{P}\right)$, as long as we don't assume them both in the same \vocab{context}.

    In this sense, intuitionistic truth is ``stronger'' than classical truth: intuitionistic truth is ``harder to come by''.


  \subsection{Intuition}

    How do we make any sense of something which is ``not false'' but also ``not true'', in a way that is somehow not contradictory?

    The answer is to focus on \vocabs{proof} instead of \vocab{truth}.

    \begin{itemize}
      \item If $P$ is true, that means $P$ is \insist{provable}.
      \item If $\negate P$ is true ($P$ is false), that means $P$ is \insist{disprovable} (the negation of provability).
      \item If $\negate \negate P$ is true ($\negate P$ is false), that means $P$ is \insist{not disprovable} (the negation of disprovability).
    \end{itemize}

    To engage in just a little bit of philosophy, what \insist{exactly} do we mean in the ``real world'' when we say something is ``provable''?

    If we're considering the ``real world'', we should consider the passage of time: there are many more things that have been ``proven'' today than there were in the year 1900. Does that mean that there are more ``provable'' things than there used to be?

    Even still today, there are ``unsolved'' problems in the fields of math and logic, like the famous \linkstyle{\href{https://en.wikipedia.org/wiki/Collatz_conjecture}{Collatz conjecture}}. Is it correct to say that the Collatz conjecture is ``either provable or disprovable'' if nobody has done either in history yet?

    This reasoning suggests a kind of problem with the word ``provable'' as it relates to the ``real world'': in general, we would need kind of omniscience or ``time-traveling'' ability in order to know what will \insist{ever} be proven. As mere mortals without time machines, we have no perfectly reliable ``test'' of ``provability''.

    What we do have is a global community of people contributing to a constantly-growing body of published proofs, which make up the sum of what we sometimes call ``mathematical knowledge''. Thanks to libraries and the internet, we might pretty reasonably test whether something has been ``proven'' or ``disproven'' by checking this body of work.

    We might then generalize informally to say that ``$P$ is proven'' means ``the global mathematical community knows of a proof of $P$''. With this perspective, we can attempt a somewhat more grounded phrasing of the interpretation from above:

    \begin{itemize}
      \item If the math community knows that $P$ is true, that means the math community knows a proof of $P$.
      \item If the math community knows that $\negate P$ is true ($P$ is false), that means the math community knows a proof that it is impossible to prove $P$.
      \item If the math community knows that $\negate \negate P$ is true ($\negate P$ is false), that means the math community knows a proof that it is impossible to disprove $P$.
    \end{itemize}

    Under this interpretation, it's perfectly natural that some propositions might be ``neither true nor false'', in the sense of actual present knowledge rather than abstract potential knowledge. Right now, the Collatz conjecture is neither known to be true nor known to be false.

    It's also clear in this interpretation that the ``truth'' of $\negate \negate P$ is consistent with the ``truth'' of $P$, but $\negate \negate P$ is a somewhat ``weaker'' claim than $P$. In principle, we might know today that it is impossible to disprove $P$, but not know today how to produce an actual complete proof of $P$. However, if we know a proof of $P$, then we certainly know it is impossible to disprove $P$ (as long as our proof theory is \vocab{internally-consistent}).

    It might still seem a little strange that we can \insist{prove} some things are neither provable nor disprovable. The status of $\negate \negate \mono{P} \subset \mono{P}$ is \insist{not} quite the same as the status of the Collatz conjecture: today we \insist{know} that $\emptyset \entails \negate \negate \mono{P} \subset \mono{P}$ is neither provable nor disprovable from the rules of \acrshort{ipl}, and that will not change in the future.

    There is quite a bit of subtlety to this, but the basic intuitive idea is that there's value in a proof system which can reason about actual present knowledge rather than abstract potential knowledge. The \vocab{independence} of $\negate \negate \mono{P} \subset \mono{P}$ from \acrshort{ipl} is a ``feature'', not a ``bug'': it reflects the actual state of our knowledge about the world, where \insist{knowledge} of $\negate \negate P$ does not necessarily imply \insist{knowledge} of $P$.

    It turns out that the way programs store and manipulate data over time is structurally similar to the way human beings reason about logical facts over time, since programming languages are foundationally based on logical theories. We'll have to distort the modern popular conception of a ``programming language'' quite a bit to get there, but we'll see soon how \acrshort{ipl}'s philosophical focus on the ``real world'' allows it to function as a \vocab{programming-language} in a very concretely meaningful way.


\input{include/footer.tex}
\end{document}
