{-
This set of notes covers our lecture discussions from weeks 9 and 10 on the
topic of natural numbers and induction.
-}

module notes14 where

{-
First, our basic STLC types again. In these definitions, we use Agda's
"data"/"where" notation to define new *logical operators* by giving the
*introduction rules* for each operator.
-}
data Unit : Set where
  unit : Unit

data Void : Set where

data _×_ (A B : Set) : Set where
  _,_ : A → B → A × B

data _⊎_ (A B : Set) : Set where
  inj₁ : A → A ⊎ B
  inj₂ : B → A ⊎ B

variable
  P Q R S : Set


{-
As we've seen with finite sets, we can also use the "data"/"where" notation to
define new *data types* by giving a set of *constructors* for the data type.
(Just like before, note that this is a somewhat different use of the word
"constructor" than in OOP.)

We begin with a definition of the set of natural numbers. To be precise, this is
just *one* definition of the natural numbers: there are many different
equivalent definitions we could give.

Intuitively, natural numbers are non-negative "whole numbers". There is some
disagreement in traditional mathematics about whether 0 or 1 should be the first
natural number, but in computer science we almost always start with 0.

This particular definition of the natural numbers is the most traditional in
mathematics. When we define the set of natural numbers this way, we sometimes
refer to them as "Peano numerals", named after the mathematician Giuseppe Peano.
-}
data ℕ : Set where
  zero : ℕ    -- zero is a constant
  suc : ℕ → ℕ -- suc is a function from ℕ to ℕ

{-
ℕ is the traditional symbol for the set of all natural numbers, which we often
pronounce "nat". The name "suc" is short for "successor", which is the
traditional name for this operator.

In our code and proofs, the zero constructor will represent the natural number
0, and the suc constructor will represent the operation of incrementing a number
(adding 1 to it).

Right now, these constructors don't really mean anything: they will get their
meanings when we define operations like addition and multiplication, which *use*
the zero and suc constructors in specific ways.

Note that Agda does not know what our names are *intended* to mean: we *could*
implement addition so that the zero constructor was treated as the number 1, or
100, and Agda would not know that anything was "wrong". In fact, this would be a
perfectly valid definition of *some* arithmetic operator, it would just be
misleading to call it "addition".

If Agda can't check that our names make sense, how can we ever know if we've
defined "addition" correctly? Well, this is what proofs are for!

There are many properties that we expect an "addition" operator to have based on
our experience with addition *outside* Agda. If we prove that these traditional
properties of addition are true for our "addition" function, we gain confidence
that our definition is actually worthy of the name "addition".
-}

{-
In general, a value of type ℕ is constructed with some number of suc
constructors applied to a final zero. The number that the value represents is
the number of suc constructors that it contains.

(Again, this isn't really true *yet* - the point is that when we define
arithmetic operators like addition and multiplication, these values will
*behave* the way that we expect the numbers 3 and 7 to behave.)
-}
three : ℕ
three = suc (suc (suc zero))

seven : ℕ
seven = suc (suc (suc (suc three)))

{-
To prove things about numbers, we'll need a way to state propositions about
equality between numbers. Like before, don't worry about the exact definition of
this type for now: just review how we've *used* the refl constructor in previous
notes.
-}
data _≡_ : ℕ → ℕ → Set where
  refl : (x : ℕ) → x ≡ x


{-
The suc constructor is a *function* which takes a natural number argument and
returns a new natural number. Because suc is a *function*, it has a definitional
property which is true for all functions in Agda:

- Congruence: (x = y) → (suc x = suc y)

Because suc is a *constructor*, it also has some special definitional properties
that are *not* true for all functions in Agda:

- Injectivity: (suc x = suc y) → (x = y)
- Generativity: suc x ≠ x

The properties of injectivity and generativity are true for all *constructors*
in Agda, but not all *functions* are injective or generative.

The properties of congruence, injectivity, and generativity are all part of
Agda's reasoning about *definitional* equalities. If we want to use them in our
proofs, we can reason about them with *propositional* equalities, by
pattern-matching as we've seen in previous notes.
-}
suc-congruent : (x : ℕ) → (y : ℕ) → (x ≡ y) → (suc x ≡ suc y)
suc-congruent .x .x (refl x) = refl (suc x)

suc-injective : (x : ℕ) → (y : ℕ) → (suc x ≡ suc y) → (x ≡ y)
suc-injective .x .x (refl (suc x)) = refl x

suc-generative : (x : ℕ) → (suc x ≡ x) → Void
suc-generative x ()


{-
Like we've seen with finite sets, we can write functions with arguments of type
ℕ by pattern-matching on them. Here is our definition of addition, with two ℕ
arguments and a ℕ return value.
-}
_+_ : ℕ → ℕ → ℕ
zero + y = y
(suc x) + y = suc (x + y)

{-
What makes this a definition of "addition", instead of something else?

First, note that both equalities we defined are valid in traditional
pencil-and-paper arithmetic:

  0 + y = y
  (1 + x) + y = 1 + (x + y)

Second, note that these two equalities cover *all possible* inputs that this
function might be given: the left argument *must* have been constructed with
either the zero constructor or the suc constructor, there are no other
possible ways to construct a value of the ℕ type.

Finally, and most subtly, note that this function *always terminates* for all
possible inputs. It never gets stuck in an infinite series of recursive calls.
This is true because the only recursive call, x + y, has a *smaller* number on
the left side than the input (suc x).
-}


{-
Here is our definition of multiplication; take time to note how it also has all
three properties mentioned above.
-}
_*_ : ℕ → ℕ → ℕ
zero * y = zero
(suc x) * y = y + (x * y)


{-
This definition of plusZeroLeft proves that zero is the "left unit" of addition:
zero plus anything is that thing.
-}
plusZeroLeft : (x : ℕ) → (zero + x) ≡ x
plusZeroLeft x = refl x

{-
Why is this a valid proof? Remember how Agda works with *definitional* equality:
it is able to reason about the two equalities that we used to *define* addition.

The first defining equality of _+_ says *literally* that zero + y = y. This
means that if we write any addition with zero as the *left* operand, the return
value will be the right operand.

This rule applies whenever we know that the *left* operand is zero, no matter
what the right operand is: the right operand could be a variable named "x", or a
variable named "Q", or the constant expression "suc (suc (suc zero))", or
anything else with type ℕ.

Since Agda knows (zero + x) = x as a *definitional* equality, we can immediately
use refl to prove it as a *propositional* equality.
-}

{-
This definition of sucIsOnePlus proves that the suc constructor actually behaves
as a "plus one" function under our addition operator.

The type of sucIsOnePlus states a proposition about *any possible* natural
number x. On paper, we would traditionally write this proposition with the ∀
operator:
  ∀ (x : ℕ). suc x ≡ (suc zero + x)
-}
sucIsOnePlus : (x : ℕ) → suc x ≡ (suc zero + x)
sucIsOnePlus x = refl (suc x)

{-
Again, Agda is reasoning about the two equalities that we used to *define*
addition. Specifically, Agda is doing this exact reasoning:

  1. suc zero + x = suc (zero + x)      by the second defining equality of _+_
  2. zero + x = x                       by the first defining equality of _+_
  3. suc (zero + x) = suc x             by 2. and the congruence of suc
  4. suc zero + x = suc x               by 1. and 3. and the transitivity of _=_

Agda knows this as a *definitional* equality, so we can immediately use refl to
prove it as a *propositional* equality.
-}


{-
In contrast, Agda does *not* know (x + zero) = zero as a definitional equality.
This means we *cannot* immediately use refl to prove plusZeroRight.
-}

plusZeroRight : (x : ℕ) → (x + zero) ≡ x
-- plusZeroRight x = refl x     -- unification error!

{-
Why not? Look at the definition of _+_ again: there is a defining equality when
the left argument is constructed with zero, or when the left argument is
constructed with suc. There is *no* defining equality when the left argument is
an arbitrary variable!

In order to make progress on the proof of plusZeroRight, we will need to
pattern-match on the argument x. This will introduce new *definitional*
equalities, and we'll have to prove two cases: one for when x = zero, and one
for when x = suc x′ for some smaller number x′.
-}
plusZeroRight zero = refl zero
plusZeroRight (suc a) = suc-congruent (a + zero) a (plusZeroRight a)

{-
If we call plusZeroRight (suc (suc (suc zero))), Agda will simplify it like
this:

  plusZeroRight (suc (suc (suc zero)))
= suc-congruent (suc (suc zero) + zero) (suc (suc zero))
    (plusZeroRight (suc (suc zero)))
= suc-congruent (suc (suc zero) + zero) (suc (suc zero))
    (suc-congruent (suc zero + zero) (suc zero)
      (plusZeroRight (suc zero)))
= suc-congruent (suc (suc zero) + zero) (suc (suc zero))
    (suc-congruent (suc zero + zero) (suc zero)
      (suc-congruent (zero + zero) zero
        (plusZeroRight zero)))
= suc-congruent (suc (suc zero) + zero) (suc (suc zero))
    (suc-congruent (suc zero + zero) (suc zero)
      (suc-congruent (zero + zero) zero
        (refl zero)))
-}

{-
This kind of pattern-matching is known as the technique of *natural induction*.
A proof by natural induction has this overall form:

  have: some property P of a natural number (e.g. P = "is even", P = "is prime")
  want: for any possible natural number x, P x is true

  goal:
    base case: P 0 is true
    inductive (step) case: for any n,
                           if P n is true,        <- "inductive hypothesis"
                           then P (n + 1) is true

  conclude: for any possible natural number x, P x is true

In the proof of plusZeroRight, the pattern goes like this:

  have: P x := ((x + zero) ≡ x)
  want: for any possible natural number x, (x + zero) ≡ x

  base case: P 0
             (zero + zero) ≡ zero
  step case: for any a,
             if (a + zero) ≡ a,          <- plusZeroRight a
             then (suc a + zero) ≡ suc a

Notice that the *recursive call* corresponds to the *inductive hypothesis*. This
is how induction "works" in a computational sense: we build a function which
recurses over its input to produce a proof.
-}

{-
On paper, we might write our inductive proof of plusZeroRight like this:

Prove that for all natural numbers x, (x + zero) ≡ x.

By the definition of the set of natural numbers, every natural number must be either zero
or the successor of another natural number.

This means that either:
  - x is zero, or
  - there exists some y where x = suc y.

We will prove this by natural induction.

If x = zero, then we need to prove (zero + zero) ≡ zero. This is true by the definition of
the _+_ function.

If x = suc y, then we need to prove (suc y + zero) ≡ suc y.
By the inductive hypothesis, we can assume (y + zero) ≡ y.
By the injectivity of suc, we have suc (y + zero) ≡ y.
By the definition of _+_, we have (suc y + zero) ≡ y.
∎
-}


{-
For the advanced reader, here are some more proofs that we partially completed
in lecture, with the "on-paper" scratch reasoning written out below the proofs.
-}
trans : (x : ℕ) → (y : ℕ) → (z : ℕ) → (x ≡ y) → (y ≡ z) → (x ≡ z)
trans x y z p q = ?

+-associativity : (x : ℕ) → (y : ℕ) → (z : ℕ) → (x + (y + z)) ≡ ((x + y) + z)
+-associativity = ?

+-commutativity : (x : ℕ) → (y : ℕ) → (x + y) ≡ (y + x)
+-commutativity = ?

+-congᴸ : (x : ℕ) → (y : ℕ) → (z : ℕ) → (y ≡ z) → ((x + y) ≡ (x + z))
+-congᴸ = ?

-- x * (y + z) = (x * y) + (x * z)
*-distributes-+ : (x : ℕ) → (y : ℕ) → (z : ℕ) → (x * (y + z)) ≡ ((x * y) + (x * z))
*-distributes-+ zero y z = refl zero
*-distributes-+ (suc x) y z =
  trans
    ((y + z) + (x * (y + z)))
    ((y + z) + ((x * y) + (x * z)))
    ((y + (x * y)) + (z + (x * z)))
    (+-congᴸ (y + z) (x * (y + z)) ((x * y) + (x * z))
      (*-distributes-+ x y z))
    ?

-- suc x * (y + z) = (y + z) + (x * (y + z))
-- (suc x * y) + (suc x * z) = (y + (x * y)) + (z + (x * z))

--   (y + (x * y)) + (z + (x * z))
-- ≡ y + (x * y) + z + (x * z)       -- by associativity of +
-- ≡ y + z + (x * y) + (x * z)       -- by commutativity of +
-- ≡ (y + z) + ((x * y) + (x * z))   -- by associativity of +

-- (y + z) + (x * (y + z)) ≡ (y + z) + ((x * y) + (x * z))    -- by congruence of +
-- (x * (y + z)) ≡ ((x * y) + (x * z))                        -- inductive hypothesis
