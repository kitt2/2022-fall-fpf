{-
There are a total of 40 points in this assignment.

In this assignment we will be working with the data types from the Agda notes,
starting with the notes numbered "11 (Agda)". All the proofs in this assignment
will be Agda code.
-}
module hwcode5 where


-- Here are our standard logical data types from simply-typed lambda calculus.
data Unit : Set where
  unit : Unit

data _×_ (A B : Set) : Set where
  _,_ : A → B → A × B

data _⊎_ (A B : Set) : Set where
  inj₁ : A → A ⊎ B
  inj₂ : B → A ⊎ B

variable
  P Q R S : Set


-- Here is our natural number data type.
data ℕ : Set where
  zero : ℕ
  suc : ℕ → ℕ

-- We've previously seen the _+_ function (addition) for natural number types;
-- here we also define the _*_ function (multiplication) in a similar way.
_+_ : ℕ → ℕ → ℕ
zero + y = y
(suc x) + y = suc (x + y)

_*_ : ℕ → ℕ → ℕ
zero * y = zero
(suc x) * y = y + (x * y)


-- Here is our propositional equality type for natural numbers.
data _≡_ : ℕ → ℕ → Set where
  refl : (x : ℕ) → x ≡ x

-- Here are some basic principles of propositional equality that we can prove.
-- You may call these functions in your own proofs in this assignment.

suc-congruent : (x : ℕ) → (y : ℕ) → (x ≡ y) → (suc x ≡ suc y)
suc-congruent .x .x (refl x) = refl (suc x)

+-congruentᴸ : (x : ℕ) → (y : ℕ) → (z : ℕ) → (x ≡ y) → (x + z) ≡ (y + z)
+-congruentᴸ .x .x z (refl x) = refl (x + z)

+-congruentᴿ : (x : ℕ) → (y : ℕ) → (z : ℕ) → (y ≡ z) → (x + y) ≡ (x + z)
+-congruentᴿ x .y .y (refl y) = refl (x + y)

+-zeroᴸ : (x : ℕ) → (zero + x) ≡ x
+-zeroᴸ x = refl x

-- Note how the "step case" of this proof calls suc-congruent.
+-zeroᴿ : (x : ℕ) → (x + zero) ≡ x
+-zeroᴿ zero = refl zero
+-zeroᴿ (suc x) = suc-congruent (x + zero) x (+-zeroᴿ x)


-- INSTRUCTIONS:
-- Fill in each hole in this file with a valid proof.

-- REQUIREMENTS:
-- You *must not* add any new top-level definitions in this file. This means you
-- must not declare any new "global" names like +-zeroᴿ or ex1-2.

-- You may introduce variable names to the left side of the = symbol in your
-- proofs, and you pattern-match by writing more than one defining equation for
-- the same proof like in +-zeroᴿ.


-- 1. Propositional logic in simply-typed lambda calculus (3 points each)

ex1-1 : (P × (Q × R)) → ((R × Q) × P)
ex1-1 = ?

ex1-2 : (P ⊎ (Q ⊎ R)) → ((R ⊎ Q) ⊎ P)
ex1-2 = ?

ex1-3 : (P → (Q → R)) → (Q → (P → R))
ex1-3 = ?


-- 2. Lambda bindings (3 points each)
-- HINT: You will need to write lambda-bindings in all of these proofs.
--       Think about where the ⊂-Intro rules would go on paper!

ex2-1 : (P → P) × (Q → Unit)
ex2-1 = ?

ex2-2 : (P → Q) ⊎ (Q → Unit)
ex2-2 = ?

ex2-3 : P → ((Unit → P) × ((P → Q) → Q))
ex2-3 = ?

ex2-4 : ((P ⊎ Q) → R) → ((P → R) × (Q → R))
ex2-4 = ?


-- 3. Propositional equality (3 points each)
-- HINT: You should not need pattern-matching for any of these proofs.
--       If you get stuck, see if there's a provided function that can help!
ex3-1 : (suc zero + suc zero) ≡ suc (suc zero)
ex3-1 = ?

ex3-2 : (x : ℕ) → (zero * x) ≡ zero
ex3-2 = ?

ex3-3 : (u : ℕ) → (suc (suc zero) * u) ≡ (u + u)
ex3-3 = ?


-- 4. Natural induction
-- HINT: You will need to pattern-match on a natural number in each of these
--       proofs. You should not have to pattern-match on more than one number!
ex4-1 : (x : ℕ) → (x * zero) ≡ zero
ex4-1 = ?

ex4-2 : (m : ℕ) → (n : ℕ) → (l : ℕ) → ((m + n) + l) ≡ (m + (n + l))
ex4-2 = ?
