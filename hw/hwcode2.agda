module hwcode2
  (⊤ : Set)
  (_∧_ : Set → Set → Set)
  (_∨_ : Set → Set → Set)
  (_⊂_ : Set → Set → Set)
  (⊤-Intro : ⊤)
  (∧-Intro : {a b : Set} → a → b → a ∧ b)
  (∧-ElimL : {a b : Set} → a ∧ b → a)
  (∧-ElimR : {a b : Set} → a ∧ b → b)
  (∨-IntroL : {a b : Set} → a → a ∨ b)
  (∨-IntroR : {a b : Set} → b → a ∨ b)
  (⊂-Elim : {a b : Set} → a ⊂ b → a → b)
  where

variable
  P Q R S : Set

ex2-1 : ⊤ ∧ ((⊤ ∨ ⊤) ∧ ⊤)
ex2-1 = ?

ex2-2 : P ∨ ((⊤ ∨ Q) ∨ (R ∨ S))
ex2-2 = ?

ex2-3 : P → Q → R → S → ((P ∧ (Q ∧ R)) ∧ S) ∧ P
ex2-3 p q r s = ?

ex2-4 : (P ∧ R) ∧ (Q ∧ S) → S ∧ (R ∧ (Q ∧ P))
ex2-4 prqs = ?

ex2-5 : ((P ⊂ R) ∧ (P ⊂ S)) ∧ P → (P ∧ Q) ∨ (R ∧ S)
ex2-5 x = ?

ex3-4 : ⊤ ∨ ((⊤ ∨ (⊤ ∨ ⊤)) ∨ ⊤)
ex3-4 = ?

ex3-5 : P → (P ⊂ Q) ∧ (P ⊂ R) → Q ∨ R
ex3-5 a b = ?
