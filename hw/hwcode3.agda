module hwcode3 where

infix 0 _∈_ _⊑_ _⊢_
infixr 1 _,_ _∷_

postulate
  Prp : Set
  Ctx : Set
  _∈_ : Prp → Ctx → Set
  _⊑_ : Ctx → Ctx → Set
  _⊢_ : Ctx → Prp → Set
  ⊤ : Prp
  _∧_ _∨_ _⊂_ : Prp → Prp → Prp
  ∅ : Ctx
  _∷_ : Prp → Ctx → Ctx
  ∈-Here : {Γ : Ctx} {a : Prp} → a ∈ a ∷ Γ
  ∈-There : {Γ : Ctx} {a b : Prp} → a ∈ Γ → a ∈ b ∷ Γ

  ⊑-Nil : {Γ : Ctx} → ∅ ⊑ Γ
  ⊑-Cons : {Γ Δ : Ctx} {a : Prp} → a ∈ Δ → Γ ⊑ Δ → a ∷ Γ ⊑ Δ
  Assumed : {Γ : Ctx} {a : Prp} → a ∈ Γ → Γ ⊢ a

  ⊤-Intro : {Γ : Ctx} → Γ ⊢ ⊤

  ∧-Intro : {Γ : Ctx} {a b : Prp} → Γ ⊢ a → Γ ⊢ b → Γ ⊢ a ∧ b
  ∧-ElimL : {Γ : Ctx} {a b : Prp} → Γ ⊢ a ∧ b → Γ ⊢ a
  ∧-ElimR : {Γ : Ctx} {a b : Prp} → Γ ⊢ a ∧ b → Γ ⊢ b

  ∨-IntroL : {Γ : Ctx} {a b : Prp} → Γ ⊢ a → Γ ⊢ a ∨ b
  ∨-IntroR : {Γ : Ctx} {a b : Prp} → Γ ⊢ b → Γ ⊢ a ∨ b
  ∨-Elim : {Γ : Ctx} {a b c : Prp} → Γ ⊢ a ∨ b → a ∷ Γ ⊢ c → b ∷ Γ ⊢ c → Γ ⊢ c

  ⊂-Intro : {Γ : Ctx} {a b : Prp} → a ∷ Γ ⊢ b → Γ ⊢ a ⊂ b
  ⊂-Elim : {Γ : Ctx} {a b : Prp} → Γ ⊢ a ⊂ b → Γ ⊢ a → Γ ⊢ b
  ⊑-Refl : {Γ : Ctx} → Γ ⊑ Γ
  ⊑-Trans : {Γ Δ Ξ : Ctx} → Γ ⊑ Δ → Δ ⊑ Ξ → Γ ⊑ Ξ

  Rearrange : {Γ Δ : Ctx} {a : Prp} → Γ ⊑ Δ → Γ ⊢ a → Δ ⊢ a
  Satisfy : {Γ : Ctx} {a b : Prp} → Γ ⊢ a → a ∷ Γ ⊢ b → Γ ⊢ b

-- Don't worry, you're not expected to understand this code yet!
-- Make sure to read the lecture notes for context.
-- There's another comment below that marks where this code ends.
data ℕ : Set where
  zero : ℕ
  suc : ℕ → ℕ

{-# BUILTIN NATURAL ℕ #-}

data ℕ* : Set where
  [] : ℕ*
  _,_ : ℕ → ℕ* → ℕ*

data Index : Ctx → ℕ → Prp → Set where
  instance
    index-zero : {Γ : Ctx} {a : Prp} → Index (a ∷ Γ) zero a
    index-suc :
      {Γ : Ctx} {a b : Prp} {n : ℕ} {{_ : Index Γ n a}} →
      Index (b ∷ Γ) (suc n) a

∈-At : {Γ : Ctx} {a : Prp} (n : ℕ) {{_ : Index Γ n a}} → a ∈ Γ
∈-At zero {{index-zero}} = ∈-Here
∈-At (suc n) {{index-suc}} = ∈-There (∈-At n)

data Indices : Ctx → ℕ* → Ctx → Set where
  instance
    indices-nil : {Γ : Ctx} → Indices ∅ [] Γ
    indices-cons :
      {Γ Δ : Ctx} {a : Prp} {n : ℕ} {ns : ℕ*}
      {{_ : Index Δ n a}} {{_ : Indices Γ ns Δ}} →
      Indices (a ∷ Γ) (n , ns) Δ

⊑-At : {Γ Δ : Ctx} (ns : ℕ*) {{_ : Indices Γ ns Δ}} → Γ ⊑ Δ
⊑-At {Γ} [] {{indices-nil}} = ⊑-Nil
⊑-At (i , is) {{indices-cons}} = ⊑-Cons (∈-At i) (⊑-At is)
-- Below here is the assignment code.

variable
  P Q R S : Prp

ex5-1 : ∅ ⊢ (P ∧ (Q ∧ R)) ⊂ ((R ∧ P) ∧ Q)
ex5-1 = ?

ex5-2 : ∅ ⊢ ((P ⊂ R) ∧ (Q ⊂ S)) ⊂ ((P ∧ Q) ⊂ (R ∧ S))
ex5-2 = ?

ex5-3 : ∅ ⊢ ((P ⊂ R) ∧ (Q ⊂ S)) ⊂ ((P ∨ Q) ⊂ (R ∨ S))
ex5-3 = ?

ex5-4 : ∅ ⊢ ((P ⊂ (Q ∧ R)) ∧ (Q ⊂ (R ⊂ S))) ⊂ (P ⊂ S)
ex5-4 = ?

ex5-5 : ∅ ⊢ ((P ⊂ Q) ⊂ R) ⊂ ((P ⊂ S) ⊂ ((S ⊂ Q) ⊂ R))
ex5-5 = ?
