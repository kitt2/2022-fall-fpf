#!/bin/bash

./ci-cd/setup.sh

apt-get install -y \
  agda \
  make \
  texlive-base \
  texlive-fonts-extra \
  texlive-latex-base texlive-latex-extra texlive-latex-recommended \
  texlive-science \
  texlive-xetex \
  xindy
