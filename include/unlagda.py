#!/usr/bin/python3

from sys import argv

with open(argv[1], "r") as file:
  code = False
  for line in file:
    if line.startswith("\\end{code}"):
      code = False
    elif line.startswith("\\begin{code}"):
      code = True
    elif code:
      print(line, end="") # line already ends in a newline
