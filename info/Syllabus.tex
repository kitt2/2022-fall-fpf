\input{include/header.tex}

\title{Syllabus}


\begin{document}

\maketitle
\tableofcontents

\newpage

\section{Disclaimer}

  This is the very first time that this course is being offered, so please be patient with me as I figure out how to teach it!

  I spent some time writing up material over Summer quarter, but I'll still be writing a lot of the material for this course during this Fall quarter. I'll publish all the material that I write as soon as I finish it so that you can read ahead when possible, but a lot of the material likely won't be published very long before it's assigned, just because it doesn't exist yet.


\section{Contact info}

  \begin{tabular}{l l}
    Email: & \href{mailto:cas28@pdx.edu}{cas28@pdx.edu} \\
    Office: & Fourth Avenue Building (FAB) 115D \\
    \href{https://fishbowl.zulip.cs.pdx.edu}{\linkstyle{Zulip}}: & \href{https://fishbowl.zulip.cs.pdx.edu/#narrow/pm-with/361-cas28}{cas28}
  \end{tabular}

  FAB 115D is in the CS offices behind the fishbowl, near the bottom-right of page 9 in the \href{https://www.pdx.edu/buildings/sites/g/files/znldhr2301/files/2021-10/Fourth\%20Avenue\%20Building\%20Floorplans.pdf}{\linkstyle{building plan}} (where L120-00 is the fishbowl).

  If you've never been to FAB before, come in the main entrance on 4\textsuperscript{th} Avenue and follow the hallway to your left until you see the Computer Science department branding on your right: that's the fishbowl.


\section{Discussion forum}

  We will be using the \href{https://fishbowl.zulip.cs.pdx.edu}{\linkstyle{Zulip}} instance hosted by the CS department as a discussion forum in this course.

  If you're registered for the course, you should receive an email invite to the \mono{\#fpf-fall-2022} channel on Zulip. If you don't get an invite, please email me and I'll add you to the channel manually.


\section{Course description}

  Don't let this description scare you! The official description of a course is like the abstract of a paper: it's not meant to be a sales pitch, it's meant to be concise and technical.

  \subsection{Summary}

    CS 410/510 Formal Proof Foundations explores the application of dependently-typed proof systems in the foundations of mathematics, using a dependently-typed programming language such as Agda, Idris, or Lean. General subjects covered are proof theory, discrete mathematics, and total functional programming; specific topics include natural deduction, structural induction, generalized algebraic data types, dependent pattern matching, and elementary set theory and number theory in a constructive setting. Additional topics may include formal languages and automata in a constructive setting, well-founded induction, coinduction, and elementary category theory.

  \subsection{Prerequisites}

    \begin{tabular}{l p{5in}}
      Required: & CS 251 or equivalent introduction to Boolean logic \\
      Recommended: & Intro-level experience with functional programming and proofs about programs, as discussed in courses like CS 311, CS 320/358, or CS 457
    \end{tabular}

  \subsection{Goals}

    Upon successful completion of this course, students will be able to:

    \begin{itemize}
      \item Interpret and construct proofs in a system of natural deduction for first-order and higher-order constructive logic.
      \item Interpret and construct proofs in a dependently-typed programming language with dependent pattern matching and structural induction.
      \item Define and prove properties of foundational concepts in constructive mathematics, including but not limited to: sets, relations, equality, functions, ordered pairs, disjoint unions, decision procedures, natural numbers, integers, lists, and setoids.
      \item Design, implement, and prove properties of simple recursive programs in a total functional programming language subject to a conservative termination checker.
    \end{itemize}

  \subsection{Resources}

    There are no required textbooks. Most of the course material will be provided on Canvas. We may refer to some parts of these freely-available resources:

    \begin{itemize}
      \item \href{https://plfa.github.io}{\linkstyle{Programming Language Foundations in Agda, Part 1: Logical Foundations}}
      \item \href{http://www.cse.chalmers.se/~ulfn/papers/afp08/tutorial.pdf}{\linkstyle{Dependently Typed Programming in Agda}}
    \end{itemize}


\section{Lecture}

  \subsection{Schedule}

    Lecture is every Monday and Wednesday, 4:40-6:30PM. See Canvas for the \href{https://canvas.pdx.edu/courses/55236/files/6024613?module\_item\_id=2837374}{\linkstyle{planned schedule of topics}}.

    Attendance is not graded, but you are expected to keep up with the weekly schedule of lectures.

  \subsection{In-person attendance}

    To attend in person, come to FAB room 10.

    To ask a question, raise your hand, and I will mute my microphone while you ask and then repeat the question myself for the recording without identifying you. (This is easier than asking each individual student whether or not they're comfortable with being recorded.)

  \subsection{Remote attendance}

    To attend remotely, join the Zoom session at \linkstyle{\url{https://pdx.zoom.us/j/87374584401}}. Please keep your camera and microphone off when you're not speaking.

    I will periodically check the Zoom chat for questions during lecture, and you can use the Zoom "raise hand" button to get my attention if you'd like to ask a question with voice and/or video.

  \subsection{Recordings}

    Each lecture will be recorded, with videos available on Canvas within 48 hours of the live lecture.

    There are some potential privacy concerns with attending a classroom that's being recorded, but I've tried to minimize them. The camera will only have a view of the front of the classroom where I'll be presenting, and I'll be speaking into a headset microphone that will not pick up soft sounds elsewhere in the room.

    The Zoom chat will not be recorded, but all unmuted voice and video over Zoom \insist{will} be recorded, because the recording is being done through Zoom on the same computer that my headset is connected to.


\section{Office hours}

  \subsection{Schedule}

    Office hours are available one-on-one or for small groups every Thursday, 4-5PM.

  \subsection{In-person attendance}

    To attend in person, come to my office in FAB room 115D.

    You'll be able to see through my office window whether I'm already helping someone. If I am, there are some chairs and desks right outside my office where you can wait.

  \subsection{Remote attendance}

    To attend remotely, join the Zoom session at \linkstyle{\url{https://pdx.zoom.us/j/87314296731}}.

    You'll be put in a Zoom waiting room at first. I'll move you from the waiting room into the meeting as soon as it's your turn in the line.


\section{Assignments}

  \subsection{Schedule}

    We will have five assignments, assigned on a two-week schedule. The first assignment will be assigned at the start of week 2, and the last will be due at the end of week 11 (``finals week'').

  \subsection{Content}

    Most assignments will have a written ``on-paper'' part and a coding part.

    In the written part of each assignment, we'll practice reasoning about proofs by manipulating traditional proof tree diagrams. It will be easiest to do this with a pencil and paper (or a tablet drawing device), but if you know \LaTeX\ you can do some of it painfully with \LaTeX.

    Some assignments will also include short answer questions with written English responses. You can write these on paper or type them in any text editor.

    In the coding part of each assignment, we'll practice reasoning about proofs by manipulating proof terms in the Agda proof language. This will require an editor with a special Agda plugin. We'll go through the setup process in the first set of lecture notes that involve Agda.

  \subsection{Submission}

    Each assignment will have a Canvas dropbox for submissions. These will remain open until the end of the course.

    If you have work on paper to submit, use a scanner or a camera to digitize it. You can scan documents in the Portland State library, and most phone cameras these days have a high enough resolution to capture writing on paper. Make sure to adjust the lighting and contrast so that all of your writing is legible.

    \insist{When you submit a file, please upload it with the exact filename that the assignment asks for.} You won't lose points for having the wrong filename, but it will make me a little grumpy; if all 50 students in the course submit with the wrong filenames, then I have to rename 50 files by hand in order to process them efficiently. (I'll be doing manual grading in this course, but I'll be using quick-and-dirty scripts to help organize my grading process.)

    Canvas may sometimes rename your files automatically---don't worry about that, just make sure you've named them correctly before you upload them.


\section{Final project}

  \subsection{Requirement}

    Students enrolled in CS 510 (at the graduate level) are required to complete a final course project.

    Students enrolled in CS 410 (at the undergraduate level) may optionally choose to complete a final course project.

    (If you don't know which you're registered for, check Banweb.)

    Each student who works on a project must complete their own individual project. You're encouraged to talk to other students about the problem that you're working on for your project, but the final work that you submit must be only your writing (with references to any sources that you used).

  \subsection{Topic}

    In the final project, you will choose a nontrivial proposition to work on proving formally. If this doesn't mean anything to you yet, this requirement will make more sense as we work through the course material.

    Your proof can be an ``on-paper'' proof tree or an Agda file (or both). You will also submit an informal description of your proof, designed for a reader who is not familiar with formal proof notation.

    You're only required to make significant progress on your proof, not to finish it. Proving can be unpredictable! Sometimes a proposition turns out to be way harder to prove than you (or I) expected it would be. If that happens, we'll find smaller sub-problems of your original problem for you to work on.

  \subsection{Schedule}

    In week 6, we'll discuss how to choose a proposition to prove. By the end of week 6, you'll submit a proposal indicating which proposition you've chosen. Once I accept your proposal, you'll have until the end of week 11 to work on your proof.


\section{Grading}

  \subsection{Assignment grading}

    Each assignment is weighted equally. You may get partial credit for showing your work even if you don't fully complete a problem.

    For students enrolled in CS 410 who choose not to complete a final project, the assignments are the only graded material in the course.

    \subsubsection{Assignment deadlines}

      Assignments will be assigned once every two weeks, and each assignment will have a ``soft due date'' of two weeks after the date it was assigned. Every soft due date is timed at 11:59PM (just before midnight).

      Assignments may sometimes be available early on Canvas, which won't change the scheduled soft due date.

      There is no grade penalty for submitting an assignment after the soft due date. The only \insist{hard} deadline for all submissions is \insist{Sunday of week 11} (the end of ``finals week''). I won't have time to grade anything submitted after that point.

      If you submit an assignment by its soft due date, you will get feedback and a grade within one week, and then you'll have one more week to resubmit your assignment \insist{once} for an updated grade. This doesn't apply to the final assignment, just because there won't be time.

      If you submit an assignment after its soft due date (and before the hard deadline at the end of the quarter), I guarantee that it will get graded for full credit before the end of the quarter---but I'll grade your submission whenever it's convenient for me, and you will not have a chance to resubmit an updated version of it. That's the deal!

      If you have a DRC accommodation for extended deadlines, that applies to the soft due dates. In that case, email me when you need an extension so that I know when to grade your submission.

  \subsection{Project grading}

    Projects will be graded on a pass/fail grading scheme. Don't worry, I plan to give you a passing project grade as long as you make some meaningful progress on your project.

    For students enrolled in CS 410 who choose to complete a final project, a passing project grade will count as one \insist{additional} 100\% grade in your assignment grades. This means your final grade will be out of six assignments instead of five. (It's not quite free bonus points, it's just adjusting your grade upwards.)

    For students enrolled in CS 510, the project will not be part of your assignment grades. Instead, a failing grade on the final project will bring your final course grade down one letter grade. This means the highest grade you can get without doing a final project is a B.


\section{Change history}

  Any changes I make to the material for this course are tracked in a GitLab repository:\\
  \linkstyle{\url{https://gitlab.cecs.pdx.edu/cas28/2022-fall-fpf}}

  You're not expected to interact with this repository, but if you ever want to know whether something has been modified, you can find the answer here.

  In general, I might modify any course material before we cover it in lecture - if you read ahead, I might have done some edits to the course material by the time we cover the topic in lecture that you read about.

  After we cover something in lecture, I'll only modify the related material if there are any corrections I need to make.

  


\end{document}
