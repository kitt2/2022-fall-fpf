Course Description:

Explores the application of dependently-typed proof systems in the foundations of mathematics, using a dependently-typed programming language such as Agda, Idris, or Lean. General subjects covered are proof theory, total functional programming, and discrete mathematics; specific topics include natural deduction, structural induction, generalized algebraic data types, dependent pattern matching, and elementary set theory and number theory in a constructive setting. Additional topics may include formal languages and automata in a constructive setting, well-founded induction, sized types, coinduction, and elementary category theory.

Prerequisites:

Required: CS 251 or equivalent

Recommended: CS 311, CS 320, CS 457

Goals:

Upon successful completion of this course, students will be able to:

- Interpret and construct proofs in a system of natural deduction for first-order and higher-order constructive logic.
- Interpret and construct proofs in a dependently-typed programming language with dependent pattern matching and structural induction.
- Define and prove properties of foundational concepts in constructive mathematics, including but not limited to: sets, relations, equality, functions, ordered pairs, disjoint unions, decision procedures, natural numbers, integers, lists, and setoids.
- Design, implement, and prove properties of simple recursive programs in a total functional programming language subject to a conservative termination checker.

Textbooks:

N/A

References:

- Programming Language Foundations in Agda, Part 1: Logical Foundations (https://plfa.github.io)
- Dependently Typed Programming in Agda (http://www.cse.chalmers.se/~ulfn/papers/afp08/tutorial.pdf)
